CC=g++
CFLAGS=-O3 -std=c++11
FILES= syncHom.cpp

syncHom: $(FILES)
	$(CC) $(FILES) $(CFLAGS) -o syncHom;
	@echo "syncHom is compiled" 
clean:
	rm -rf syncHom