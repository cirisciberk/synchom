#ifndef _NAIVE_H_
#define _NAIVE_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include "global.h"
#include "limits.h"
#include <cfloat>

using namespace std;

enum AlgorithmType { topDown, bottomUp, hybrid };
enum CudaOpt { naive, memory, warp };

template<bool memoryOpt>
void synchronizing_check(int *a, int N, int P, int *distance) {
	int* levels = new int[200];
	memset(levels, 0, 200 * sizeof(int));

	for (int i = 0; i < N; ++i) {
		for (int j = 0; j <= i; ++j) {
			int id;
			if (memoryOpt)
				id = Id(i, j);
			else
				id = IdNaive(i, j, N);

			if (distance[id] == -1) {
				for (int p = 0; p < P; p++) {
					int ts1 = a[p + i * P];
					int ts2 = a[p + j * P];
					int tid;
					if (memoryOpt)
						tid = Id(ts1, ts2);
					else
						tid = IdNaive(ts1, ts2, N);
					cout << "tid " << tid << ": distance is " << distance[tid] << endl;
				}

				cout << "automata is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
				out << endl;
				exit(0);
				return;
			}
			else {
				levels[distance[id]]++;
			}
		}
	}

#ifdef DEBUG
	int lvl = 0;
	while (levels[lvl] > 0) {
		cout << "lvl " << lvl++ << ": " << levels[lvl] << endl;
	}
	cout << endl;
#endif

#if LOG_LEVEL & DATA_ANALYSIS
	out << levels[1];
	lvl = 2;
	while (levels[lvl] > 0) {
		out << "-" << levels[lvl++];
	}
#endif
	free(levels);
}

template<bool memoryOpt>
long long costPhi(int *a, int *distance, int *letter, int *actives, int *active_marker, int N, int P, int id, int no_actives, int &step)
{
	while (distance[id] > 0) {
		int let = letter[id];

		for (int i = 0; i < no_actives; i++) {
			actives[i] = a[let + actives[i] * P];
		}

		int s1, s2;

		if (memoryOpt) {
			s1 = s1fromId(id);
			s2 = s2fromId(id, s1);
			id = Id(a[let + s1 * P], a[let + s2 * P]);
		}
		else {
			s1 = id / N;
			s2 = id % N;
			id = IdNaive(a[let + s1 * P], a[let + s2 * P], N);
		}
	}

	//reduce the number of active states
	int active_count = 0;
	for (int i = 0; i < no_actives; i++) {
		int act = actives[i];
		if (active_marker[act] != step) {
			actives[active_count++] = act;
			active_marker[act] = step;
		}
	}
	no_actives = active_count;
	step++;

#if ALGORITHM == SP || ALGORITHM == PL || ALGORITHM == FR
	long long finalTotalDist = 0;
	for (int i = 0; i < no_actives; i++) {
		for (int j = 0; j < i; j++) {
			int s1 = actives[i];
			int s2 = actives[j];

			int tid;
			if (memoryOpt)
				tid = Id(s1, s2);
			else
				tid = IdNaive(s1, s2, N);

			finalTotalDist += distance[tid];
		}
	}

	return finalTotalDist;
#elif ALGORITHM == CR
	return no_actives;
#endif
}

template<bool memoryOpt>
void greedyHeuristic_finding(int *a, int *distance, int *letter, int *actives, int * active_marker, int N, int P, PNode* &path) {

#if ALGORITHM == SP
	cout << "The algorithm is SynchroP" << endl;
#elif ALGORITHM == PL
	cout << "The algorithm is SynchroPL" << endl;
#elif ALGORITHM == CR
	cout << "The algorithm is Cardinality" << endl;
#elif ALGORITHM == FR
	cout << "The algorithm is SynchroP-Fractional" << endl;
#endif

	PNode* last = NULL;
	memset(active_marker, 0, sizeof(int) * N);

	int no_actives = N;
	for (int i = 0; i < N; ++i) {
		actives[i] = i;
	}

	int* cp_actives = new int[N];

	int step = 1;
	while (no_actives > 1) {
		//cout << "no active states is " << no_actives << endl;
		//find the pair id with minimum phi-cost value 
		unsigned long long int min_cost = LLONG_MAX;

		int min_id;
		for (int i = 0; i < no_actives; i++) {
			for (int j = 0; j < i; j++) {
				int s1 = actives[i];
				int s2 = actives[j];

				int id;
				if (memoryOpt)
					id = Id(s1, s2);
				else
					id = IdNaive(s1, s2, N);

				memcpy((void*)cp_actives, (void*)actives, sizeof(int) * N);
        unsigned long long int cost = costPhi<memoryOpt>(a, distance, letter, cp_actives, active_marker, N, P, id, no_actives, step);

#if ALGORITHM == PL
				cost += distance[id];
#endif

#if ALGORITHM == FR
				cost = cost * distance[id];
#endif

				if (min_cost > cost) {
					min_cost = cost;
					min_id = id;
				}
			}
		}
		cout << min_id << " " << min_cost << " ";

#if LOG_LEVEL & DATA_ANALYSIS
		if (max_of_min_distances < distance[min_id])
			max_of_min_distances = distance[min_id];
		min_distance_counter++;
		min_distance_sum += distance[min_id];
#endif
		// cout << "merging pair from level " << min_distance << endl;

		//apply the path and store it
		int pid = min_id;
		int added = 0;
		while (distance[pid] > 0) {
			int let = letter[pid];
			insertToPath(let, path, last);
			added++;

			for (int i = 0; i < no_actives; i++) {
				actives[i] = a[let + actives[i] * P];
			}

			int s1, s2;

			if (memoryOpt) {
				s1 = s1fromId(pid);
				s2 = s2fromId(pid, s1);
				pid = Id(a[let + s1 * P], a[let + s2 * P]);
			}
			else {
				s1 = pid / N;
				s2 = pid % N;
				pid = IdNaive(a[let + s1 * P], a[let + s2 * P], N);
			}
		}

		cout << added << endl;

		//reduce the number of active states
		int active_count = 0;
		for (int i = 0; i < no_actives; i++) {
			int act = actives[i];
			if (active_marker[act] != step) {
				actives[active_count++] = act;
				active_marker[act] = step;
			}
		}
		no_actives = active_count;
		step++;
	}


	free(cp_actives);
}

//a is automata a[i][j] -> state j goes to a[i][j] with letter j
//iap is inverse automata pointers -> ia[i][iap[i][j] ... ia[i][j+1]] keeps the state ids which go to state j with letter i
//there are N states and p letters in the automata
void greedyHeuristic_naive(int* a, int* iap, int* ia, int N, int P, PNode* &path) {
	int noOfPair = (N * (N + 1)) / 2;
	int* actives = new int[N];

	int* distance = new int[noOfPair];
	int* letter = new int[noOfPair];
	int* que = new int[noOfPair];

	int* active_marker = new int[noOfPair];

#ifdef TIMER
	double t1 = omp_get_wtime();
	double total = 0;
#endif

#if LOG_LEVEL & DATA_ANALYSIS
	int max_of_min_distances = 0;
	int min_distance_counter = 0;
	int min_distance_sum = 0;
#endif


	for (int i = 0; i < noOfPair; i++) {
		distance[i] = -1;
	}

	//BFS queue for the pairs
	int qs = 0;
	int qe = 0;

	for (int i = 0; i < N; ++i) {
		int id = Id(i, i);
		distance[id] = 0;
		que[qe++] = id;
	}

	//there are more nodes in the queue
	while (qs < qe) {
		int q_id = que[qs++];
		int q_dist = distance[q_id];

		//will process the pair with id q_id now
		int q_s1 = s1fromId(q_id); //the first state in the pair
		int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

#ifdef DEBUG
		cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << q_dist << endl;
#endif

		int* p_ia = ia; //this is the inverse automata for letter p
		int* p_iap = iap; //and its state pointers

		for (int p = 0; p < P; p++) {

			for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < p_iap[q_s1 + 1]; ++iap_s1_ptr) {
				int ia_s1 = p_ia[iap_s1_ptr];
				for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < p_iap[q_s2 + 1]; ++iap_s2_ptr) {
					int ia_s2 = p_ia[iap_s2_ptr];
					int ia_id = Id(ia_s1, ia_s2);
					if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
						distance[ia_id] = q_dist + 1;
						letter[ia_id] = p;
						que[qe++] = ia_id;
					}
				}
			}
			p_ia += N; //this is the inverse automata for letter p
			p_iap += (N + 1); //and its state pointers
		}
	}
#ifdef TIMER
	double time = omp_get_wtime() - t1;
	total += time;
	cout << "BFS tree generation takes " << time << " seconds\n";
#if LOG_LEVEL & TIME_ANALYSIS
	out << time << " ";
#endif
#endif

	synchronizing_check<true>(a, N, P, distance);

#ifdef TIMER
	t1 = omp_get_wtime();
#endif

	greedyHeuristic_finding<true>(a, distance, letter, actives, active_marker, N, P, path);

#ifdef TIMER
	time = omp_get_wtime() - t1;
	//cout << "Path generation takes " << time << " seconds\n";
	total += time;
	cout << "The heuristic takes " << total << " seconds\n";
#if LOG_LEVEL & TIME_ANALYSIS
	out << total << " ";
#endif
#endif
#if LOG_LEVEL & DATA_ANALYSIS
	out << " " << (N * (N - 1)) / 2 << " " << lvl - 1 << " "
		<< max_of_min_distances << " " << float(min_distance_sum) / min_distance_counter;
#endif
	free(distance);
	free(letter);
	free(que);
	free(actives);
	free(active_marker);
}

int findNoOfNextLevelEdge(int * iap, int *distance, int noOfPair, int P, int N, int next_level) {
	int result = 0;
#pragma omp parallel for reduction(+:result)
	for (int q_id = 0; q_id < noOfPair; ++q_id) {
		if (distance[q_id] == next_level) {
			int q_s1 = s1fromId(q_id); //the first state in the pair
			int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair

			int *p_iap = iap;
			for (int p = 0; p < P; p++) {
				result += (p_iap[q_s1 + 1] - p_iap[q_s1]) * (p_iap[q_s2 + 1] - p_iap[q_s2]);
				p_iap += (N + 1);
			}
		}
	}
	return result;
}

int findNoOfNextLevelVertex(int * iap, int *distance, int noOfPair, int P, int N, int next_level) {
	int result = 0;
#pragma omp parallel for reduction(+:result)
	for (int q_id = 0; q_id < noOfPair; ++q_id) {
		if (distance[q_id] == next_level) {
			result++;
		}
	}
	return result;
}

void bfs_step_topdown(int*ia, int*iap, int*letter, int P, int noOfPair, int N, int *distance, int frontier, bool& isupdated) {
#pragma omp parallel for
	for (int q_id = 0; q_id < noOfPair; ++q_id) {
		if (distance[q_id] == frontier) {
			//will process the pair with id q_id now
			int q_s1 = s1fromId(q_id); //the first state in the pair
			int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair

#ifdef DEBUG
			cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << frontier << endl;
#endif
			int* p_ia = ia;//this is the inverse automata for letter p
			int* p_iap = iap;//and its state pointers
			for (int p = 0; p < P; p++) {
				for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < p_iap[q_s1 + 1]; ++iap_s1_ptr) {
					int ia_s1 = p_ia[iap_s1_ptr];
					for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < p_iap[q_s2 + 1]; ++iap_s2_ptr) {
						int ia_s2 = p_ia[iap_s2_ptr];

						int ia_id = Id(ia_s1, ia_s2);
						if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
							distance[ia_id] = frontier + 1;
							letter[ia_id] = p;
							isupdated = true;
						}
					}
				}
				p_ia += N;
				p_iap += N + 1;
			}
		}
	}
}

void bfs_step_bottomup(int*a, int*letter, int P, int noOfPair, int N, int *distance, int frontier, bool& isupdated) {
#pragma omp parallel for
	for (int q_id = 0; q_id < noOfPair; ++q_id) {
		if (distance[q_id] < 0) {
			//will process the pair with id q_id now
			int q_s1 = s1fromId(q_id); //the first state in the pair
			int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair

#ifdef DEBUG
			cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << frontier << endl;
#endif

			for (int p = 0; p < P; p++) {

				int a_s1 = a[p + q_s1 * P];
				int a_s2 = a[p + q_s2 * P];
				int a_id = Id(a_s1, a_s2);
				if (distance[a_id] == frontier) { //we found an unvisited pair. so we need to add this to the queue
					distance[q_id] = frontier + 1;
					letter[q_id] = p;
					isupdated = true;
					break;
				}
			}
		}
	}
}



void greedyHeuristic_naive_parallel_level_based(int* a, int* iap, int* ia, int N, int P, PNode* &path, bool is_top_down) {
	int noOfPair = N * (N + 1) / 2;
	int* actives = new int[N];

	int* distance = new int[noOfPair];
	int* letter = new int[noOfPair];

	int* active_marker = new int[noOfPair];

#ifdef TIMER
	double t1 = omp_get_wtime();
	double total = 0;
#endif
#if LOG_LEVEL & LEVEL_ANALYSIS
	static bool isPrinted = false;
#endif

	int noOfRemainingVertex = N * (N - 1) / 2;
	int noOfNextLevelVertex = N;
	int noOfRemainingEdge = noOfRemainingVertex * P;
	int noOfNextLevelEdge = 0;
	for (int i = 0; i < N; ++i) {
		for (int p = 0; p < P; p++) {
			int temp = iap[p * (N + 1) + i + 1] - iap[p * (N + 1) + i];
			noOfNextLevelEdge += temp * (temp - 1) / 2;
		}
	}
	noOfRemainingEdge = noOfRemainingEdge - noOfNextLevelEdge;

	for (int i = 0; i < noOfPair; i++) {
		distance[i] = -1;
	}

	for (int i = 0; i < N; ++i) {
		int id = Id(i, i);
		distance[id] = 0;
	}

	bool isupdated;
	int frontier = 0;
	//there are more nodes in the queue
	do {
		cout << "frontier level " << setw(2) << frontier << ":"
			<< setw(10) << noOfNextLevelEdge
			<< setw(10) << noOfRemainingEdge
			<< setw(10) << noOfNextLevelVertex
			<< setw(10) << noOfRemainingVertex;
#if LOG_LEVEL & LEVEL_ANALYSIS
		if (!isPrinted)
			out << noOfNextLevelEdge << " " << noOfRemainingEdge << " " << noOfNextLevelVertex << " " << noOfRemainingVertex << " ";
#endif
#ifdef TIMER
		double tstep = omp_get_wtime();

#endif
		isupdated = false;
		if (is_top_down)
			bfs_step_topdown(ia, iap, letter, P, noOfPair, N, distance, frontier, isupdated);
		else
			bfs_step_bottomup(a, letter, P, noOfPair, N, distance, frontier, isupdated);
#ifdef TIMER
		tstep = omp_get_wtime() - tstep;
		cout << " " << setw(6) << tstep;
#if LOG_LEVEL & LEVEL_ANALYSIS
		out << tstep << " ";
#endif
#endif
		cout << endl;
		noOfNextLevelEdge = findNoOfNextLevelEdge(iap, distance, noOfPair, P, N, ++frontier);
		noOfRemainingEdge -= noOfNextLevelEdge;

		noOfNextLevelVertex = findNoOfNextLevelVertex(iap, distance, noOfPair, P, N, frontier);
		noOfRemainingVertex -= noOfNextLevelVertex;
	} while (isupdated);
#if LOG_LEVEL & LEVEL_ANALYSIS
	out << "|||||||,";
	isPrinted = true;
#endif

#ifdef TIMER
	double time = omp_get_wtime() - t1;
	total += time;
	cout << "BFS tree generation takes " << time << " seconds\n";

#if LOG_LEVEL & TIME_ANALYSIS
	out << time << " ";
#endif
#endif

	synchronizing_check<true>(a, N, P, distance);

#ifdef TIMER
	t1 = omp_get_wtime();
#endif

	greedyHeuristic_finding<true>(a, distance, letter, actives, active_marker, N, P, path);

#ifdef TIMER
	time = omp_get_wtime() - t1;
	//cout << "Path generation takes " << time << " seconds\n";
	total += time;
	cout << "The heuristic takes " << total << " seconds\n";
#if LOG_LEVEL & TIME_ANALYSIS
	out << total << " ";
#endif
#endif
	free(distance);
	free(letter);
	free(actives);
	free(active_marker);

}

void bfs_step_topdown_with_queue(int*ia, int*iap, int*queue, int P, int N, int qe, int next_level, int offset, int lq_size, int *distance, int*letter, int*local_queue, bool*isupdated, int& end) {
#pragma omp for schedule(dynamic, 256)
	for (int i = 0; i < qe; ++i) {
		int q_id = queue[i];
		if (isupdated[q_id]) continue;
		isupdated[q_id] = true;
		//will process the pair with id q_id now
		int q_s1 = s1fromId(q_id); //the first state in the pair
		int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

#ifdef DEBUG
		cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << next_level << endl;
#endif

		int* p_ia = ia; //this is the inverse automata for letter p
		int* p_iap = iap; //and its state pointers

		for (int p = 0; p < P; p++) {
			for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < p_iap[q_s1 + 1]; ++iap_s1_ptr) {
				int ia_s1 = p_ia[iap_s1_ptr];
				for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < p_iap[q_s2 + 1]; ++iap_s2_ptr) {
					int ia_s2 = p_ia[iap_s2_ptr];
					int ia_id = Id(ia_s1, ia_s2);
					if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
						distance[ia_id] = next_level;
						letter[ia_id] = p;
						local_queue[offset + end] = ia_id;
						end++;
						if (end > lq_size) {
							cout << "Error! Local Size exceeded!" << lq_size << " " << end << endl;
							exit(1);
						}
					}
				}
			}
			p_ia += N;
			p_iap += N + 1;
		}
	}
}

void bfs_step_bottomup_with_queue(int*a, int*queue, int P, int N, int qe, int next_level, int offset, int lq_size, int *distance, int*letter, int*local_queue, int& end) {
#pragma omp for schedule(dynamic, 256)
	for (int i = 0; i < qe; ++i) {
		int q_id = queue[i];
		//will process the pair with id q_id now
		int q_s1 = s1fromId(q_id); //the first state in the pair
		int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

#ifdef DEBUG
		cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << next_level << endl;
#endif
		int p;
		for (p = 0; p < P; p++) {

			int a_s1 = a[p + q_s1 * P];
			int a_s2 = a[p + q_s2 * P];
			int a_id = Id(a_s1, a_s2);
			if (distance[a_id] == next_level - 1) { //we found an unvisited pair. so we need to add this to the queue
				distance[q_id] = next_level;
				letter[q_id] = p;
				break;
			}

		}
		if (p == P) {
			local_queue[offset + end] = q_id;
			end++;
			if (end > lq_size) {
				cout << "Error! Local Size exceeded!" << lq_size << " " << end << endl;
				exit(1);
			}
		}
	}
}

void inline switch_queue(int * distance, int noOfPair, int lq_size, int offset, int *local_queue, int &end) {
	end = 0;
#pragma omp for schedule(dynamic, 256)
	for (int i = 0; i < noOfPair; ++i) {
		if (distance[i] < 0) {
			local_queue[offset + end] = i;
			end++;
			if (end > lq_size) {
				cout << "Error! Local Size exceeded!" << lq_size << " " << end << endl;
				exit(1);
			}
			//			#pragma omp critical 
			//cout << end << " " << omp_get_thread_num() <<  " " << lq_size << endl; 
		}
	}
}

template<AlgorithmType aType>
void greedyHeuristic_naive_parallel_queue_based(int* a, int* iap, int* ia, int N, int P, PNode* &path, int num_thread) {

	int noOfPair = N * (N + 1) / 2;
	int* actives = new int[N];

	int* distance = new int[noOfPair];
	int* letter = new int[noOfPair];

	int* active_marker = new int[noOfPair];

	int* queue = new int[noOfPair];
	int* local_queue = new int[noOfPair * 2];
	bool* isupdated = new bool[noOfPair];
	int lq_size = noOfPair * 2 / num_thread;
	int qe = 0;

	int treshold, frontierVertices;
	bool isSwitched; //flag for switching from top down to bottom up
	isSwitched = false;

#ifdef TIMER
	double t1 = omp_get_wtime();
	double total = 0;
#endif

	if (aType != bottomUp) {
		for (int i = 0; i < noOfPair; i++) {
			distance[i] = -1;
		}

		for (int i = 0; i < N; ++i) {
			int id = Id(i, i);
			distance[id] = 0;
			queue[qe++] = id;
		}
		if (aType == hybrid)
			treshold = noOfPair - qe;

	}
	else {
		for (int i = 0; i < noOfPair; i++) {
			distance[i] = -1;
		}

		//first pair
		int prev_id = 0;
		distance[0] = 0;

		//other i,i pairs
		for (int i = 1; i < N; ++i) {
			int id = Id(i, i);
			distance[id] = 0;

			for (int j = prev_id + 1; j < id; ++j) {
				queue[qe++] = j;
			}

			prev_id = id;
		}
	}
	memset(isupdated, false, sizeof(bool) * noOfPair);
	int next_level = 1;
	//there are more nodes in the queue
	while (0 < qe) {

#ifdef TIMER
		double tstep = omp_get_wtime();
#endif

#pragma omp parallel
		{
			int end = 0;
			int offset = lq_size * omp_get_thread_num();

			if (aType == topDown)
				bfs_step_topdown_with_queue(ia, iap, queue, P, N, qe, next_level, offset, lq_size, distance, letter, local_queue, isupdated, end);
			else if (aType == bottomUp)
				bfs_step_bottomup_with_queue(a, queue, P, N, qe, next_level, offset, lq_size, distance, letter, local_queue, end);
			else {
				if (isSwitched)
					bfs_step_bottomup_with_queue(a, queue, P, N, qe, next_level, offset, lq_size, distance, letter, local_queue, end);
				else {
					bfs_step_topdown_with_queue(ia, iap, queue, P, N, qe, next_level, offset, lq_size, distance, letter, local_queue, isupdated, end);

					// calculating new queue size
					// which is approximately equal to number of vertices at frontier level
					if (0 == offset)
						frontierVertices = 0;
#pragma omp barrier

#pragma omp atomic
					frontierVertices += end;
#pragma omp barrier

					if (0 == offset)
						treshold -= frontierVertices;
#pragma omp barrier

					//if number of vertices at frontier level is bigger than remaining
					//then we need to change queue to prepare for bottom up
					if (treshold <= frontierVertices) {//change queue
						switch_queue(distance, noOfPair, lq_size, offset, local_queue, end);
#pragma omp barrier
						isSwitched = true;
					}

				}

			}


			if (0 == offset) {
				qe = 0;
			}

#pragma omp barrier

#pragma omp critical
			{
				memcpy(queue + qe, local_queue + offset, end * sizeof(int));
				qe += end;
			}
		}
		next_level++;

#ifdef TIMER
		tstep = omp_get_wtime() - tstep;
#ifdef DEBUG
		cout << "frontier level " << setw(2) << next_level - 2 << ":" << " " << setw(6) << tstep << endl;
#endif
#if LOG_LEVEL & LEVEL_ANALYSIS
		out << tstep << " ";
#endif
#endif
	}

#if LOG_LEVEL & LEVEL_ANALYSIS
	out << "||||||| ";
#endif

#ifdef TIMER
	double time = omp_get_wtime() - t1;
	total += time;
	cout << "BFS tree generation takes " << time << " seconds\n";

#if LOG_LEVEL & TIME_ANALYSIS
	out << time << " ";
#endif
#endif

	synchronizing_check<true>(a, N, P, distance);

#ifdef TIMER
	t1 = omp_get_wtime();
#endif

	greedyHeuristic_finding<true>(a, distance, letter, actives, active_marker, N, P, path);

#ifdef TIMER
	time = omp_get_wtime() - t1;
	//cout << "Path generation takes " << time << " seconds\n";
	total += time;
	cout << "The heuristic takes " << total << " seconds\n";

#if LOG_LEVEL & TIME_ANALYSIS
	out << total << " ";
#endif
#endif
	free(distance);
	free(letter);
	free(actives);
	free(active_marker);
	free(isupdated);
	free(queue);
	free(local_queue);

}

/*
template <CudaOpt c>
__global__ void bfs_step_top_down_cuda(int*ia, int*iap, int*letter, int P, int noOfPair, int N, int *distance, int frontier, bool* isupdated) {

int blockID = blockIdx.x + blockIdx.y * gridDim.x;
int tid = threadIdx.x + blockDim.x * blockID;
int q_id;
int pStart, pEnd;
if (c == warp) {
pStart = tid % P;
pEnd = pStart + 1;
q_id = tid / P;
}
else {
pStart = 0;
pEnd = P;
q_id = tid;
}
if (q_id < noOfPair && distance[q_id] == frontier) {
//will process the pair with id q_id now
int q_s1, q_s2;
if (c == naive) {
q_s1 = q_id / N;
q_s2 = q_id % N;
}
else {
q_s1 = s1fromId(q_id); //the first state in the pair
q_s2 = s2fromId(q_id, q_s1);//the second state in the pair (we are sure that q_s1 >= q_s2)
}

for (int p = pStart; p < pEnd; ++p) {
int* p_ia = &(ia[p * N]); //this is the inverse automata for letter p
int* p_iap = &(iap[p * (N + 1)]); //and its state pointers
for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < p_iap[q_s1 + 1]; ++iap_s1_ptr) {
int ia_s1 = p_ia[iap_s1_ptr];
for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < p_iap[q_s2 + 1]; ++iap_s2_ptr) {
int ia_s2 = p_ia[iap_s2_ptr];

int ia_id;
if (c == naive)
ia_id = IdNaive(ia_s1, ia_s2, N);
else
ia_id = Id(ia_s1, ia_s2);
if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
distance[ia_id] = frontier + 1;
letter[ia_id] = p;
*isupdated = true;
}
}
}
}
}
}

template <CudaOpt c>
__global__ void bfs_step_bottom_up_cuda(int*a, int*letter, int P, int noOfPair, int N, int *distance, int frontier, bool* isupdated) {

int blockID = blockIdx.x + blockIdx.y * gridDim.x;
int tid = threadIdx.x + blockDim.x * blockID;
int q_id;
int pStart, pEnd;
if (c == warp) {
pStart = tid % P;
pEnd = pStart + 1;
q_id = tid / P;
}
else {
pStart = 0;
pEnd = P;
q_id = tid;
}
if (q_id < noOfPair && distance[q_id] < 0) {
//will process the pair with id q_id now
int q_s1, q_s2;
if (c == naive) {
q_s1 = q_id / N;
q_s2 = q_id % N;
}
else {
q_s1 = s1fromId(q_id); //the first state in the pair
q_s2 = s2fromId(q_id, q_s1);//the second state in the pair (we are sure that q_s1 >= q_s2)
}

for (int p = pStart; p < pEnd; ++p) {
int a_s1 = a[p + q_s1 * P];
int a_s2 = a[p + q_s2 * P];
int a_id;
if (c == naive)
a_id = IdNaive(a_s1, a_s2, N);
else
a_id = Id(a_s1, a_s2);

if (distance[a_id] == frontier) { //we found an unvisited pair. so we need to add this to the queue
distance[q_id] = frontier + 1;
letter[q_id] = p;
*isupdated = true;
break;
}
}

}
}

__global__ void find_frontier_vertex_cuda(int *distance, int noOfPair, int frontier, int * result) {
int blockID = blockIdx.x + blockIdx.y * gridDim.x;
int q_id = threadIdx.x + blockDim.x * blockID;
if (q_id < noOfPair && distance[q_id] == frontier) {
atomicAdd(result, 1);
}

}

template <CudaOpt c, AlgorithmType aType>
void greedyHeuristic_naive_cuda(int* a, int* iap, int* ia, int N, int P, PNode* &path) {

int noOfPair;
if (c == naive)
noOfPair = N * N;
else
noOfPair = N * (N + 1) / 2;

int* actives = new int[N];

int* distance = new int[noOfPair];
int* letter = new int[noOfPair];

int *distance_d, *letter_d, *iap_d, *ia_d, *a_d;

bool isupdated;
bool *isupdated_d;
cudaCheck(cudaMalloc((void **)&isupdated_d, sizeof(bool)));

int frontierVertex, *frontierVertex_d, treshold;

treshold = N * (N - 1) / 2;
frontierVertex = N;
cudaCheck(cudaMalloc((void **)&frontierVertex_d, sizeof(int)));

int frontier = 0;

cudaCheck(cudaMalloc((void **)&distance_d, sizeof(int)*noOfPair));
cudaCheck(cudaMalloc((void **)&letter_d, sizeof(int)*noOfPair));

int* active_marker = new int[noOfPair];

if (aType != bottomUp) {

cudaCheck(cudaMalloc((void **)&iap_d, sizeof(int) * (N + 1)*P));
cudaCheck(cudaMalloc((void **)&ia_d, sizeof(int)*N * P));

cudaCheck(cudaMemcpy(iap_d, iap, sizeof(int) * (N + 1) * P, cudaMemcpyHostToDevice));
cudaCheck(cudaMemcpy(ia_d, ia, sizeof(int)*N * P, cudaMemcpyHostToDevice));

}
if (aType != topDown) {

cudaCheck(cudaMalloc((void **)&a_d, sizeof(int)*N * P));
cudaCheck(cudaMemcpy(a_d, a, sizeof(int)*N * P, cudaMemcpyHostToDevice));

}

#ifdef TIMER
double t1 = omp_get_wtime();
double total = 0;
#endif

for (int i = 0; i < noOfPair; i++) {
distance[i] = -1;
}

for (int i = 0; i < N; ++i) {

int id;
if (c == naive)
id = IdNaive(i, i, N);
else
id = Id(i, i);

distance[id] = 0;
}

cudaCheck(cudaMemcpy(distance_d, distance, sizeof(int)*noOfPair, cudaMemcpyHostToDevice));
//there are more nodes in the queue
int dim;
if (c == warp)
dim = ceil(sqrt(noOfPair * P / 256.0));
else
dim = ceil(sqrt(noOfPair / 256.0));
dim3 gridDim(dim, dim);
do {
#ifdef TIMER
#if LOG_LEVEL & LEVEL_ANALYSIS
double tstep = omp_get_wtime();
#endif
#endif
isupdated = false;
cudaCheck(cudaMemcpy(isupdated_d, &isupdated, sizeof(bool), cudaMemcpyHostToDevice));
if (aType == topDown)
bfs_step_top_down_cuda<c> << < gridDim, 256 >> > (ia_d, iap_d, letter_d, P, noOfPair, N, distance_d, frontier, isupdated_d);
else if (aType == bottomUp)
bfs_step_bottom_up_cuda<c> << < gridDim, 256 >> > (a_d, letter_d, P, noOfPair, N, distance_d, frontier, isupdated_d);
else {
if (frontierVertex < treshold) {
bfs_step_top_down_cuda<c> << < gridDim, 256 >> > (ia_d, iap_d, letter_d, P, noOfPair, N, distance_d, frontier, isupdated_d);

cudaMemset(frontierVertex_d, 0, sizeof(int));
find_frontier_vertex_cuda << < gridDim, 256 >> >(distance_d, noOfPair, frontier + 1, frontierVertex_d);

cudaCheck(cudaMemcpy(&frontierVertex, frontierVertex_d, sizeof(int), cudaMemcpyDeviceToHost));
treshold -= frontierVertex;
}
else {
bfs_step_bottom_up_cuda<c> << < gridDim, 256 >> > (a_d, letter_d, P, noOfPair, N, distance_d, frontier, isupdated_d);
}

}


cudaCheck(cudaMemcpy(&isupdated, isupdated_d, sizeof(bool), cudaMemcpyDeviceToHost));
#ifdef TIMER
#if LOG_LEVEL & LEVEL_ANALYSIS
tstep = omp_get_wtime() - tstep;
cout << "frontier level " << frontier << "\t:" << tstep << endl;
#endif
#endif
frontier++;
} while (isupdated);
cudaDeviceSynchronize();
#ifdef TIMER
double time = omp_get_wtime() - t1;
total += time;
cout << "BFS tree generation takes " << time << " seconds\n";

#if LOG_LEVEL & TIME_ANALYSIS
out << time << ",";
#endif
#endif

cudaCheck(cudaMemcpy(distance, distance_d, sizeof(int)*noOfPair, cudaMemcpyDeviceToHost));
cudaCheck(cudaMemcpy(letter, letter_d, sizeof(int)*noOfPair, cudaMemcpyDeviceToHost));

synchronizing_check < c != naive >(a, N, P, distance);

#ifdef TIMER
t1 = omp_get_wtime();
#endif

greedyHeuristic_finding < c != naive >(a, distance, letter, actives, active_marker, N, P, path);

#ifdef TIMER
time = omp_get_wtime() - t1;
//cout << "Path generation takes " << time << " seconds\n";
total += time;
cout << "The heuristic takes " << total << " seconds\n";

#if LOG_LEVEL & TIME_ANALYSIS
out << total << ",";
#endif
#endif
free(distance);
free(letter);
free(actives);
free(active_marker);

cudaCheck(cudaFree(distance_d));
cudaCheck(cudaFree(letter_d));
cudaCheck(cudaFree(isupdated_d));
cudaCheck(cudaFree(frontierVertex_d));

if (aType != bottomUp) {
cudaCheck(cudaFree(iap_d));
cudaCheck(cudaFree(ia_d));
}
if (aType != topDown) {
cudaCheck(cudaFree(a_d));
}

}
*/
#endif //_NAIVE_H_
