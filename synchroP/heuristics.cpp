#define SP 1
#define PL 2
#define CR 3
#define FR 4

#ifndef ALGORITHM
#define ALGORITHM SP
#warning Algorithm not given explicitly, SynchroP "SP" assumed
#endif

#include <iostream>
#include <fstream>
#include <cmath>
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "omp.h"
#include "global.h"
#include "naive.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

int checkInverse(int *a, int* iap, int* ia, int N, int P) {
	for (int p = 0; p < P; p++) {
		for (int i = 0; i < N; i++) {
			int target = a[p + i * P];

			int found = 0;
			for (int iaptr = iap[p * (N + 1) + target]; iaptr < iap[p * (N + 1) + target + 1]; ++iaptr) {
				int incoming = ia[p * N + iaptr];
				if (i == incoming) {
					found = 1;
					break;
				}
			}

			if (!found) {
				cout << "something is wrong " << i << " goes to " << target << " with " << p << " but it is not in the inverse automata\n";
				exit(1);
			}
		}
	}

	for (int p = 0; p < P; p++) {
		for (int i = 0; i < N; i++) {
			for (int iaptr = iap[p * (N + 1) + i]; iaptr < iap[p * (N + 1) + i + 1]; ++iaptr) {
				int source = ia[p * N + iaptr];
				if (a[p + source * P] != i) {
					cout << "something is wrong " << i << " has " << source << " in inverse automata but it " << source << " goes to " << a[p + source * P] << " with " << p << "\n";
					exit(1);
				}
			}
		}
	}

	return 0;
}

int main(int argc, char** argv) {
	if (argc != 4) {
		cout << "Usage: " << argv[0] << " no_states alphabet_size rand_seed\n" << endl;
		return 1;
	}
	int N = atoi(argv[1]);
	int P = atoi(argv[2]);
	uint seed = atoi(argv[3]);

#ifdef LOG_LEVEL
	char* filename = new char[256];

	struct stat st = { 0 };

#if ALGORITHM == SP
	sprintf(filename, "results_synchroP/%s_%s", argv[1], argv[2]);
	if (stat("results_synchroP", &st) == -1) {
		mkdir("results_synchroP", 0700);
	}
#elif ALGORITHM == PL
	sprintf(filename, "results_synchroPL/%s_%s", argv[1], argv[2]);
	if (stat("results_synchroPL", &st) == -1) {
		mkdir("results_synchroPL", 0700);
	}
#elif ALGORITHM == CR
	sprintf(filename, "results_cardinality/%s_%s", argv[1], argv[2]);
	if (stat("results_cardinality", &st) == -1) {
		mkdir("results_cardinality", 0700);
	}
#elif ALGORITHM == FR
	sprintf(filename, "results_fractional/%s_%s", argv[1], argv[2]);
	if (stat("results_fractional", &st) == -1) {
		mkdir("results_fractional", 0700);
	}
#else
#error Algorithm is not properly defined to be one of these: "SP", "PL", "CR", "FR"
#endif

#if LOG_LEVEL & TIME_ANALYSIS
	sprintf(filename, "%s_time_and_length", filename);
#endif
#if LOG_LEVEL & DATA_ANALYSIS
	sprintf(filename, "%s_data", filename);
#endif
#if LOG_LEVEL & LEVEL_ANALYSIS
	sprintf(filename, "%s_level", filename);
#endif
	sprintf(filename, "%s.csv", filename);

	out.open(filename, ios::app);
	free(filename);
#endif

	int* automata = new int[P * N];

#pragma omp parallel
	{
		uint tseed = (omp_get_thread_num() + 1) * (seed + 1912812);
#pragma omp for schedule(static)
		for (int i = 0; i < P * N; ++i) {
			automata[i] = ((int)rand_r(&tseed)) % N;
		}
	}

#ifdef DEBUG
	printAutomata(automata, N, P);
#endif

	/*cout << P << " "<< N << endl;
	for(int i = 0; i < N; ++i) {
	for(int j = 0; j < P; ++j) {
	cout << automata[j][i] << " ";
	}
	cout << endl;
	}*/

	int* inv_automata_ptrs = new int[P * (N + 1)];
	int* inv_automata = new int[P * N];

#pragma omp parallel for schedule(static)
	for (int i = 0; i < P; ++i) {

		int *a = &(automata[i]);
		int *ia = &(inv_automata[i * N]);
		int *iap = &(inv_automata_ptrs[i * (N + 1)]);

		memset(iap, 0, sizeof(int) * (N + 1));
		for (int j = 0; j < N; j++) { iap[a[j * P] + 1]++; }
		for (int j = 1; j <= N; j++) { iap[j] += iap[j - 1]; }
		for (int j = 0; j < N; j++) { ia[iap[a[j * P]]++] = j; }
		for (int j = N; j > 0; j--) { iap[j] = iap[j - 1]; } iap[0] = 0;
	}

	checkInverse(automata, inv_automata_ptrs, inv_automata, N, P);

#ifdef DEBUG
	printInverseAutomata(inv_automata_ptrs, inv_automata, N, P);
#endif

	PNode *path;
	//sequential version
	if (true) {
		cout << "sequential:" << endl;
		path = NULL;
		greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N, P, path);

		pathPrinter(automata, path, N, P);
	}

#ifdef LOG_LEVEL
	out << endl;
	out.close();
#endif

	return 0;
}
