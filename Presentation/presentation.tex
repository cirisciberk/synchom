% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. x the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.



\documentclass{beamer}

%
% DO NOT USE THIS FILE AS A TEMPLATE FOR YOUR OWN TALKS�!!
%
% Use a file in the directory solutions instead.
% They are much better suited.
%


% Setup appearance:

\usetheme{Darmstadt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}

\setbeamertemplate{frametitle continuation}{}

% Standard packages
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage{array}
\usepackage[T1]{fontenc}

\usepackage[lined,ruled,vlined]{algorithm2e}
\usepackage{algorithmic,float}

\usepackage{ulem}

% Setup TikZ

\usepackage{tikz}
\usetikzlibrary{fit}
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]

\setbeamertemplate{bibliography item}[text]

\newcommand{\cerny}{\v{C}ern\'y}

% Author, Title, etc.

\title[Block Partitioning and Perfect Phylogenies] 
{%
  Using Synchronizing Heuristics to Construct Homing Sequences%
}

\author[B. Cirisci]
{
  \textbf{Berk Cirisci}, M. Yusa Emek, Ege Sorguc, Kamer Kaya and Husnu Yenigun
}

\institute[Sabanci Univ]
{
 Sabanci University
}

\date[January 22, 2019]{January 22, 2019}


% The main document

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}[allowframebreaks]{Outline}
\scriptsize
  \tableofcontents
\end{frame}

\section{Definitions and Examples}

\subsection{Finite Automaton}

\begin{frame}{Definition of a Finite Automaton}
\begin{center}
\begin{tikzpicture}[->,>=stealth',auto,node distance=25mm, thick,main node/.style={circle,fill=blue!20,draw}]

  \node[main node] (1) {$q_1$};
  \node[main node] (2) [right of=1] {$q_2$};

  \path[every node/.style={font=\sffamily\small}]
    (1) edge [out= -45, in= 225] node [pos=0.2,below] {$a$} (2)
        edge [out=  45, in= 135] node [pos=0.2,above] {$b$} (2)
        edge [loop left] node {a} (1)
    (2) edge node [pos=0.5,above] {$b$} (1)
        edge [loop right] node {$a$} (2);
\end{tikzpicture}
\end{center}
\begin{block}{Definition}
A \alert{finite automaton $A$} is defined by a tuple \alert{$A=(Q,X,\delta)$} where (for this figure)
\begin{itemize}
\item $Q = \{q_1, q_2 \}$: finite non-empty set of states
\item $X = \{ a, b \}$ : finite non-empty set of input symbols
\item $\delta \subseteq Q \times X \times Q$ : a transition (next state) relation
\end{itemize}
\footnotesize(For complete \& deterministic automata, $\delta: Q \times X \rightarrow Q$ is a total function.) \endgraf
\normalsize When the current state is $q_1$, if the input symbol $b$ is applied, the automaton has to move into state $q_2$.
\end{block}
\end{frame}

\subsection{Finite State Machine}

\begin{frame}{Definition of an FSM}
\begin{center}
\begin{tikzpicture}[->,>=stealth',auto,node distance=25mm, thick,main node/.style={circle,fill=blue!20,draw}]

  \node[main node] (1) {$q_1$};
  \node[main node] (2) [right of=1] {$q_2$};

  \path[every node/.style={font=\sffamily\small}]
    (1) edge [out= -45, in= 225] node [pos=0.2,below] {$a/0$} (2)
        edge [out=  45, in= 135] node [pos=0.2,above] {$b/0$} (2)
        edge [loop left] node {a/1} (1)
    (2) edge node [pos=0.5,above] {$b/1$} (1)
        edge [loop right] node {$a/0$} (2);
\end{tikzpicture}
\end{center}
\begin{block}{Definition}
A \alert{finite state machine $M$} is defined by a quintuple \alert{$M=(Q,X,Y,\delta, \lambda)$} where (for this figure)
\begin{itemize}
\item $Q = \{q_1, q_2 \}$: finite non-empty set of states
\item $X = \{ a, b \}$ : finite non-empty set of input symbols
\item $Y = \{0, 1\}$ : finite non-empty set of output symbols
\item $\delta: Q \times X \rightarrow Q$ next state function
\item $\lambda: Q \times X \rightarrow Y$: output function (for Mealy Machine)
\end{itemize}
\end{block}
\end{frame}

\subsection{Applying Sequence to a Finite State Machine}

\begin{frame}{Applying a word to a state}

\begin{center}
	\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={circle,fill=blue!20,draw}]
	
	\node[main node] (1) {$q_1$};
	\node[main node] (2) [below of=1] {$q_2$};
	\node[main node] (3) [right of=1  , node distance=20mm] {$q_3$};
	
	\path[every node/.style={font=\sffamily\small}]
	(1) edge [below] node [above] {$a / 0$} (3)
	edge [out= -45 ,in=-135] node[below] {$b / 1$} (3)
	(2) edge [loop left] node{$a / 0 $} (2)
	edge [left] node {$b / 1$} (1)
	(3) edge [out= 135 ,in=45] node[above] {$a / 1$} (1)
	edge [out= 20 ,in= -20, loop] node [right] {$b / 0 $}  (2);	
	\end{tikzpicture}
\end{center}

\begin{block}{Walking with sequences}
When \alert{a sequence $w$ is applied to a state $q$}, we take the walk that starts
from $q$ and along the path labeled by $w$ producing $o$.\\
\pause
For example, $\delta(q_2,{\color{blue}bab}) = q_3$ and $\lambda(q_2,{\color{blue}bab}) = 100$.
\end{block}
\begin{center}
	\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={circle,fill=blue!20,draw}]
	
	\node[main node] (1) {$q_2$};
	\node[main node] (2) [right of=1] {$q_1$};
	\node[main node] (3) [right of=2] {$q_3$};
	\node[main node] (4) [right of=3] {$q_3$};
	
	\path[every node/.style={font=\sffamily\small}]
	(1) edge node [above] {$b/1$} (2)
	(2) edge node [above] {$a/0$} (3)
	(3) edge node [above] {$b/0$} (4);
	\end{tikzpicture}
\end{center}
\end{frame}

\subsection{Synchronizing and Homing Sequences}

\begin{frame}{Definition of a Synchronizing Sequence}
\begin{center}
\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={circle,fill=blue!20,draw}]

  \node[main node] (1) {$q_1$};
  \node[main node] (2) [below left of=1] {$q_2$};
  \node[main node] (3) [below right of=2] {$q_3$};
  \node[main node] (4) [below right of=1] {$q_4$};

  \path[every node/.style={font=\sffamily\small}]
    (1) edge node [left] {$a$} (4)
        edge node [above] {$b$} (2)
    (2) edge node {$a$} (4)
        edge [loop left] node {$b$} (2)
    (3) edge node [right] {$a$} (2)
        edge [out=0,in=-90] node[right] {$b$} (4)
    (4) edge node [left] {$a$} (3)
        edge [out=90,in=0] node[right] {$b$} (1);
\end{tikzpicture}
\hspace*{1cm}
\onslide<2->\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={scale = 0.5,circle,fill=blue!20,draw}]
	
	\node[main node] (1) {$q_1$};
	\node[main node] (2) [right of=1] {$q_2$};
	\node[main node] (3) [right of=2] {$q_2$};
	\node[main node] (4) [right of=3] {$q_2$};
	\node[main node] (5) [below of = 1]{$q_2$};
	\node[main node] (6) [below of = 2] {$q_2$};
	\node[main node] (7) [below of = 3]{$q_2$};
	\node[main node] (8) [below of = 4]{$q_2$};
	\node[main node] (9) [below of = 5]{$q_3$};
	\node[main node] (10) [below of = 6] {$q_4$};
	\node[main node] (11) [below of = 7]{$q_1$};
	\node[main node] (12) [below of = 8]{$q_2$};
	\node[main node] (13) [below of = 9]{$q_4$};
	\node[main node] (14) [below of = 10] {$q_1$};
	\node[main node] (15) [below of = 11]{$q_2$};
	\node[main node] (16) [below of = 12]{$q_2$};
	
	
	\path[every node/.style={font=\sffamily\small}]
	(1) edge node [above, scale = 0.7] {$b$} (2)
	(2) edge node [above, scale = 0.7] {$b$} (3)
	(3) edge node [above, scale = 0.7] {$b$} (4)
	(5) edge node [above, scale = 0.7] {$b$} (6)
	(6) edge node [above, scale = 0.7] {$b$} (7)
	(7) edge node [above, scale = 0.7] {$b$} (8)
	(9) edge node [above, scale = 0.7] {$b$} (10)
	(10) edge node [above, scale = 0.7] {$b$} (11)
	(11) edge node [above, scale = 0.7] {$b$} (12)
	(13) edge node [above, scale = 0.7] {$b$} (14)
	(14) edge node [above, scale = 0.7] {$b$} (15)
	(15) edge node [above, scale = 0.7] {$b$} (16);
	
	
\end{tikzpicture}
\end{center}
\onslide<1->\begin{block}{Synchronizing Sequences (Reset Words)}
A sequence $w$ is a \alert{synchronizing sequence} for an automaton $A=(Q, X, \delta)$ if
$\delta(Q,w)$ is singleton.\\
For example:\\
$\bullet$ {\color{blue}$ababba$} is synchronizing sequence. All states merge at {$q_4$}\\
$\bullet$ {\color{blue}$bbb$} is also synchronizing sequence. All states merge at {$q_2$}
\end{block}
\end{frame}

\begin{frame}{Definition of a Homing Sequence}
\begin{center}
	\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={circle,fill=blue!20,draw}]
	
	\node[main node] (1) {$q_1$};
	\node[main node] (2) [below of=1] {$q_2$};
	\node[main node] (3) [right of=1  , node distance=20mm] {$q_3$};
	
	\path[every node/.style={font=\sffamily\small}]
	(1) edge [below] node [above] {$a / 0$} (3)
	edge [out= -45 ,in=-135] node[below] {$b / 1$} (3)
	(2) edge [loop left] node{$a / 0 $} (2)
	edge [left] node {$b / 1$} (1)
	(3) edge [out= 135 ,in=45] node[above] {$a / 1$} (1)
	edge [out= 20 ,in= -20, loop] node [right] {$b / 0 $}  (2);	
	\end{tikzpicture}
\end{center}

\begin{block}{Homing Sequences}
A sequence $w$ is \alert{homing sequence} for FSM $M=(Q,X,Y,\delta,\lambda)$ if
final state of $M$ can be determined from the output produced in response to $w$. \pause For example: {\color{blue}$aa$} is a homing sequence.
\begin{itemize}
\item \begin{center} \footnotesize $\delta(q_1, aa) = q_1, \;\; \lambda(q_1, aa) = 01$ \end{center} 
\item \begin{center} \footnotesize $\delta(q_2, aa) = q_2, \;\; \lambda(q_2, aa) = 00$ \end{center} 
\item \begin{center} \footnotesize $\delta(q_3, aa) = q_3, \;\; \lambda(q_3, aa) = 10$ \end{center} 
\end{itemize}
\end{block}
\end{frame}

\section{Relation Between HS and SS}

\subsection{Goal: Using SS to find HS}
\begin{frame}{How to reach our goal?}
\begin{center}
\begin{block}{Bad News...}
It is NP-hard to compute a shortest SS and HS. 
\end{block}
\pause
\begin{block}{Our Motivation}
Unlike existing heuristics to find short synchronizing sequences~\cite{eppstein}, homing heuristics are not widely studied.
\end{block}
\pause
\begin{block}{Good News...}
We discover a relation between synchronizing and homing sequences by creating an automaton called homing automaton.
\end{block}
\pause
\begin{block}{Method}
By applying synchronizing heuristics on this automaton we get short homing sequences.
\end{block}
\end{center}
\end{frame}

\subsection{Homing Automaton}
\begin{frame}{Example}
\begin{center}
	\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={circle,fill=blue!20,draw}]
	
	\node[main node] (1) {$q_1$};
	\node[main node] (2) [below of=1] {$q_2$};
	\node[main node] (3) [right of=1  , node distance=20mm] {$q_3$};
	
	\path[every node/.style={font=\sffamily\small}]
	(1) edge [below] node [above] {$a / 0$} (3)
	edge [out= -45 ,in=-135] node[below] {$b / 1$} (3)
	(2) edge [loop below] node{$a / 0 $} (2)
	edge [left] node {$b / 1$} (1)
	(3) edge [out= 90 ,in=90] node[above] {$a / 1$} (1)
	edge [out= 20 ,in= -20, loop] node [right] {$b / 0 $}  (2);
	\end{tikzpicture}
	\hspace*{1cm}
	\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={circle,fill=blue!20,draw}]
	
	\node[main node] (1) [scale = 0.5]{$\{q_1,q_2\}$};
	\node[main node] (2) [below left of=1,scale = 0.5  , node distance=20mm] {$\{q_1,q_3\}$};
	\node[main node] (3) [below right of = 1,scale = 0.5  , node distance=20mm] {$\{q_2,q_3\}$};
	\node[main node] (4) [below left of = 3  , node distance=20mm] {$q^\star$};
	
	\path[every node/.style={font=\sffamily\small}]
	(1) edge node [above left] {$b$} (2);
	\path[every node/.style={font=\sffamily\small}]
	(1) edge node[above right] {$a$} (3);
	
	\path[every node/.style={font=\sffamily\small}]
	(2) edge node [below left] {$a$} (4);
	\path[every node/.style={font=\sffamily\small}]
	(2) edge node [above right] {$b$} (4);
	
	\path[every node/.style={font=\sffamily\small}]
	(3) edge node [below right] {$a $} (4);
	\path[every node/.style={font=\sffamily\small}]
	(3) edge node [above left] {$b $} (4);
	
	\path[every node/.style={font=\sffamily\small}]
	(4) edge [loop below]  node [below] {$a , b$} (4);	
	\end{tikzpicture}
\end{center}

\end{frame}

\begin{frame}{Construction of Homing Automaton}
\begin{block}{Definition}
Let $M=(Q,X,Y,\delta, \lambda)$ be an FSM. The homing automaton (HA) $A\textsubscript{M}$ of $M$ is an automaton $A\textsubscript{M} =(Q\textsubscript{A},X,\delta \textsubscript{A})$ which is constructed as follows:
\begin{itemize}
\item[--] The set $Q\textsubscript{A}$ consists of all 2-element subsets of $Q$ and an extra state $q^\star$. ($Q\textsubscript{A}  = \{\{q\textsubscript{i} ,q\textsubscript{j}\} | q\textsubscript{i} ,q\textsubscript{j} \in Q \land q\textsubscript{i} \neq q\textsubscript{j}\} \cup \{q\star\}$). \\
\item[--] The transition function $\delta\textsubscript{A}$ is defined as follows:
\begin{itemize}
\item For an input symbol $i \in X, \delta\textsubscript{A}(q^\star,i) = q^\star$
\item For $q = \{q_i,q_j\} \in Q_A$ and an input symbol $i \in X$, \\
\begin{itemize}
\item[--] If $\delta(q_i, i) = \delta(q_j, i)$ then $\delta_A(q, i) = q^\star$
\item[--] If $\lambda(q_i, i) \neq \lambda(q_j, i)$ then $\delta_A(q, i) = q^\star$
\item[--] Otherwise, $\delta_A(q, i) = \{\delta(q_i, i), \delta(q_j, i) \}$
\end{itemize}
\end{itemize}
\end{itemize}
\end{block}
\end{frame}

\subsection{How to Generate FSM from HA}
\begin{frame}{Previous Example}
\begin{center}
	\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={circle,fill=blue!20,draw}]
	
	\node[main node] (1) {$q_1$};
	\node[main node] (2) [below of=1] {$q_2$};
	\node[main node] (3) [right of=1  , node distance=20mm] {$q_3$};
	
	\path[every node/.style={font=\sffamily\small}]
	(1) edge [below] node [above] {$a / 0$} (3)
	edge [out= -45 ,in=-135] node[below] {$b / 1$} (3)
	(2) edge [loop below] node{$a / 0 $} (2)
	edge [left] node {$b / 1$} (1)
	(3) edge [out= 90 ,in=90] node[above] {$a / 1$} (1)
	edge [out= 20 ,in= -20, loop] node [right] {$b / 0 $}  (2);
	\end{tikzpicture}
	\hspace*{1cm}
	\pause
	\begin{tikzpicture}[->,>=stealth',auto,node distance=15mm, thick,main node/.style={circle,fill=blue!20,draw}]
	
	\node[main node] (1) [scale = 0.5]{$\{q_1,q_2\}$};
	\node[main node] (2) [below left of=1,scale = 0.5  , node distance=20mm] {$\{q_1,q_3\}$};
	\node[main node] (3) [below right of = 1,scale = 0.5  , node distance=20mm] {$\{q_2,q_3\}$};
	\node[main node] (4) [below left of = 3  , node distance=20mm] {$q^\star$};
	
	\pause
	\path[every node/.style={font=\sffamily\small}]
	(4) edge [loop below]  node [below] {$a , b$} (4);	
	
	\pause
	\path<5->[every node/.style={font=\sffamily\small}]
	(1) edge node [above left] {$b$} (2);
	\path<4->[every node/.style={font=\sffamily\small}]
	(1) edge node[above right] {$a$} (3);
	
	\path<6->[every node/.style={font=\sffamily\small}]
	(2) edge node [below left] {$a$} (4);
	\path<7->[every node/.style={font=\sffamily\small}]
	(2) edge node [above right] {$b$} (4);
	
	\path<8->[every node/.style={font=\sffamily\small}]
	(3) edge node [below right] {$a $} (4);
	\path<9->[every node/.style={font=\sffamily\small}]
	(3) edge node [above left] {$b $} (4);
	
	\end{tikzpicture}
\end{center}

\end{frame}

\subsection{Why does it work?}

\begin{frame}{The Relation}
\begin{block}{Theorem}
Let $M = (Q,X,Y,\delta,\lambda)$ be an FSM and $A_M = (Q_A,X,\delta_A)$ be the HA of $M$. An input sequence $w \in X^\star$ is an HS for $M$ iff $w$ is an SS for $A_M$.
\end{block}
\pause
\begin{block}{Proof}
If $w$ is an HS of $M$, for any two states $q_i$ and $q_j$ in $M$, $\lambda(q_i, w) \neq \lambda(q_j, w)$ or $\delta(q_i, w) = \delta(q_j, w)$. Therefore $\delta_A(\{q_i,q_j\}, w) = q^\star$ for any $\{q_i,q_j\} \in Q_A$. For $q^\star, \delta_A(q^\star, w) = q^\star$. Hence $w$ is an SS for $A_M$.
\end{block}
\pause
\begin{block}{Good News...}
Please recall that an SS may not exist for an automaton in general. However, for a homing automaton of a minimal FSM, an SS will always exist due to Theorem above.
\end{block}
\end{frame}

\subsection{Analysis of Using Homing Automaton}

\begin{frame}{Complexity Analysis}
\begin{block}{Complexities of Original SS Heuristics}
The heuristics that we use for our experiments are Greedy $O(n^3+pn^2)$~\cite{eppstein} and SynchroP $O(n^5+pn^2)$~\cite{Roman09} where $n$ is the number of states and $p$ is the number of inputs.
\end{block}
\pause
\begin{block}{Complexities with Homing Automaton}
Constructing the homing automaton $A_M$ of $M = (Q,X,Y,\delta,\lambda)$ would require $O(pn^2)$ time. Note that the number of states of $A_M$ is $1 + n(n-1)/2$ , and $A_M$ still has $p$ input symbols. Therefore, applying Greedy to $A_M$ requires $O(pn^4 + n^6)$ time, and applying SynchroP to $A_M$ requires $O(pn^4 +n^{10})$.
\end{block}
\pause
\begin{block}{That means...}
It is not practical to use HA for finding homing sequences
\end{block}
\end{frame}

\section{Computing HS without HA}

\subsection{General Algorithm for Synchronizing Sequence}

\begin{frame}{Pseudocode of Computing SS}
\begin{block}{Algorithm}
\normalem
\begin{algorithm}[H]
\algsetup{linenosize=\tiny}
   \scriptsize
 \KwIn{An FSM $M=(Q,X,Y,\delta, \lambda), \tau\{q_i, q_j\}$ \tcp{$ \tau\{q_i, q_j\}$ is SS for $q_i, q_j$}}
 \KwOut{A synchronizig sequence $\mathrm{T}$ for $M$} %tree $\mathbb{A}$ with $\midS\mid$ leaf nodes in the constructed partial EST}
\Begin{
\lnl{InRes2} $D \leftarrow Q^2_d$ \tcp{$D$ will track the set of d-pairs (different)}
\lnl{InRes2} $\mathrm{T} \leftarrow \varepsilon$ \tcp{$\mathrm{T}$ will accumulate the homing sequence}
\lnl{InRes2}\While(\texttt{// as long as we have a pair to be merged}){$D \neq \emptyset$} 
 {
    \lnl{InRes2} Pick two different active states $q_i$ and $q_j$ from $D$ according to \textbf{{\color{red} heuristic}}\\
    \lnl{InRes2} \For {$\{q_k,q_l\}\in D$}
    {
    	 \lnl{InRes2} \If {$|\delta(\{q_k,q_l\},\tau{\{q_i,q_j\}})|\neq 1$}
	 {
	 	\lnl{InRes2} insert $\delta(\{q_k,q_l\},\tau{\{q_i,q_j\}})$ into $D'$
	 }
    }    
    \lnl{InRes2} $\mathrm{T} = \mathrm{T} \tau{\{q_i,q_j\}}$ \tcp{append $\tau{\{q_i,q_j\}}$ to $\mathrm{T}$}  
    \lnl{InRes2} $D \leftarrow D'$ \tcp{update $D$ accordingly}
}
}
\end{algorithm}
\ULforem
\end{block}

\end{frame}

\subsection{General Algorithm for Homing Sequence}

\begin{frame}{Computing HS Without Using HA}
\begin{block}{Algorithm}
\normalem
\begin{algorithm}[H]
\algsetup{linenosize=\tiny}
   \scriptsize
 \KwIn{An FSM $M=(Q,X,Y,\delta, \lambda), \gamma\{q_i, q_j\}$ \tcp{$ \gamma\{q_i, q_j\}$ is HS for $q_i, q_j$}}
 \KwOut{A homing sequence $\Gamma$ for $M$} %tree $\mathbb{A}$ with $\midS\mid$ leaf nodes in the constructed partial EST}
\Begin{
\lnl{InRes2} $D \leftarrow Q^2_d$ \tcp{$D$ will track the set of d-pairs (different)}
\lnl{InRes2} $\Gamma \leftarrow \varepsilon$ \tcp{$\Gamma$ will accumulate the homing sequence}
\lnl{InRes2}\While(\texttt{// as long as we have a pair to be homed}){$D \neq \emptyset$} 
 {
    \lnl{InRes2} Pick two different active states $q_i$ and $q_j$ from $D$ according to \textbf{{\color{red} heuristic}}\\
    \lnl{InRes2} \For {$\{q_k,q_l\}\in D$}
    {
    	 \lnl{InRes2} \If {$|\delta(\{q_k,q_l\},\gamma{\{q_i,q_j\}})|\neq 1$ \textbf{and} $\lambda(q_k,\gamma{\{q_i,q_j\}})= \lambda(q_l,\gamma{\{q_i,q_j\}})$}
	 {
	 	\lnl{InRes2} insert $\delta(\{q_k,q_l\},\gamma{\{q_i,q_j\}})$ into $D'$ 
	 }
    }    
    \lnl{InRes2} $\Gamma = \Gamma \gamma{\{q_i,q_j\}}$ \tcp{append $\gamma{\{q_i,q_j\}}$ to $\Gamma$}  
    \lnl{InRes2} $D \leftarrow D'$ \tcp{update $D$ accordingly}
}
}
\end{algorithm}
\ULforem
\end{block}

\end{frame}

\subsection{More Details about the General Algorithm}
\begin{frame}{Computing HS Without Using HA}
\begin{block}{Finding Homing Sequences}
\footnotesize $\gamma\{q_i, q_j\}$ for all $\{q_i, q_j\} \in D$ can be computed by using BFS as the computation of $\tau\{q_i, q_j\}$ with a small modification in which a $\{q_i,q_j\}, i \neq j$ that gives different outputs for its states is also located at level 1 (root) of BFS forest.
\end{block}
\pause
\begin{block}{Fast-HS}
\footnotesize Select a random pair from $D$
\end{block}
\pause
\begin{block}{Greedy-HS}
\footnotesize Select the pair from $D$ with the shortest homing sequence
\end{block}
\pause
\begin{block}{SynchroP-HS}
\footnotesize Select the pair from $D$ with the minimum cost $\varPhi$ where $\varPhi(D)=\sum_{\{q_i,q_j \}\in D}|\gamma{\{q_i,q_j\}}|$ and $|\gamma{\{q_i,q_j\}}|$ is the HS for $\{q_i, q_j\}$
\end{block}
\end{frame}

\section{Results}
\subsection{Heuristics with HA}
\begin{frame}{Experiments for Using Synchronizing Heuristics on HA}
\begin{center}
\vspace*{-0.8cm}\includegraphics[scale = 0.25]{withHA.png}
\end{center}
\end{frame}

\subsection{Heuristics without HA}
\begin{frame}{Experiments with Homing Heuristics on FSMs}
\begin{center}
\vspace*{-0.5cm}\includegraphics[scale = 0.25]{withoutHA-small.png}
\end{center}
\end{frame}

\begin{frame}{Experiments on Larger State Sizes}
\begin{center}
\vspace*{-1cm}
\hspace*{-0.5cm}\includegraphics[scale = 0.25]{withoutHA-large.png}
\vspace*{0.3cm}
\begin{block}{Analysis of Tables}
\begin{itemize}
    \item[--] HA based heuristics can't scale to large state sizes.
    \item[--] Sequence lengths and total times from heuristics without HS are promising compared to shortest.
    \item[--] Fast-HS is becoming faster than Greedy-HS when the FSM's with large state sizes are considered.
\end{itemize} 
\end{block}
\end{center}
\end{frame}

\section{Summary}

\begin{frame}
  \frametitle<presentation>{Summary}

  \begin{itemize}
   \item Finding a shortest reset word is hard but there exist heuristics for computing short reset sequences.
  \item A synchronizing sequence $w$ is an HS for an FSM $M$ iff $w$ is an SS for HA $A_M$.
  \item We can find homing sequence using synchronizing heuristics on homing automaton.
    \item Homing automaton has the number of states squared compared to the number of states of the corresponding FSM therefore it is not practical to use.
    \item Inspired by the homing automaton, a new technique can be applied on FSM directly with some modification on synchronizing heuristics to find short homing sequences.
  \end{itemize} 

\end{frame}

\begin{frame}
\begin{center}
\LARGE Thank you \endgraf
\vspace*{5mm}
\huge ANY QUESTIONS?
\end{center}
\end{frame}

\begin{frame}[allowframebreaks]{References}
\scriptsize


\bibliographystyle{plain}
\bibliography{ss}

\end{frame}

\end{document}


