#include <iostream>
#include "string.h"
#include <climits>
#include <math.h>
#include <algorithm>
#include <queue>
#include <stdint.h>
#include <fstream>

using std::cout;
using std::cin;
using std::sort;
using std::endl;
using std::priority_queue;
using std::vector;
using std::ifstream;
using std::string;

//#define PRINTAUTOMATA
//#define PRINTPATH
//#define SIZEOFPATH
//#define DEBUGSYNC
#define JUSTBOUNDS

#define Id(s1, s2) ((s1 > s2)?(((s1 * (s1 + 1))/2) + s2):(((s2 * (s2 + 1))/2) + s1)) //this is how we compute the ids
#define IdNaive(s1, s2, N) ((s1 > s2)?((s1 * N) + s2):((s2 * N) + s1)) //this is how we compute the ids
#define s1fromId(id) ((int)(sqrt((2.0 * id) +1.0) - 0.5));
#define s2fromId(id, s1) (id - ((s1 * (s1 + 1))/2));

//struct for priority queue
template <typename T> 
struct DistID {
  T id;
  T dist;
  T label;
  DistID(T i, T d, T l) : id(i), dist(d), label(l) {}

  bool operator<(const DistID& rhs) const {
    if (dist > rhs.dist)
      return true;
    else if (dist < rhs.dist)
      return false;
    else {
      if (label > rhs.label)
        return true;
      else
        return false;
    }
  }
};

template <bool memoryOpt>
bool synchronizing_check(int* a, int* iap, int* ia, int N, int P) {
  int noOfPair = (N * (N + 1)) / 2;

  int* distance = new int[noOfPair];
  int* letter = new int[noOfPair];
  int* que = new int[noOfPair];

  for (int i = 0; i < noOfPair; i++) {
    distance[i] = -1;
  }

  //BFS queue for the pairs
  int qs = 0;
  int qe = 0;

  for (int i = 0; i < N; ++i) {
    int id = Id(i, i);
    distance[id] = 0;
    que[qe++] = id;
  }

  //there are more nodes in the queue
  while (qs < qe) {
    int q_id = que[qs++];
    int q_dist = distance[q_id];

    //will process the pair with id q_id now
    int q_s1 = s1fromId(q_id); //the first state in the pair
    int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

    int* p_ia = ia; //this is the inverse automata for letter p
    int* p_iap = iap; //and its state pointers

    for (int p = 0; p < P; p++) {
      for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < p_iap[q_s1 + 1]; ++iap_s1_ptr) {
        int ia_s1 = p_ia[iap_s1_ptr];
        for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < p_iap[q_s2 + 1]; ++iap_s2_ptr) {
          int ia_s2 = p_ia[iap_s2_ptr];
          int ia_id = Id(ia_s1, ia_s2);
          if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
            distance[ia_id] = q_dist + 1;
            letter[ia_id] = p;
            que[qe++] = ia_id;
          }
        }
      }
      p_ia += N; //this is the inverse automata for letter p
      p_iap += (N + 1); //and its state pointers
    }
  }
  int* levels = new int[N * (N+1) / 2];
  memset(levels, 0, N * (N + 1) / 2 * sizeof(int));
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j <= i; ++j) {
      int id;
      if (memoryOpt)
        id = Id(i, j);
      else
        id = IdNaive(i, j, N);

      if (distance[id] == -1) {
        for (int p = 0; p < P; p++) {
          int ts1 = a[p * N + i];
          int ts2 = a[p * N + j];
          int tid;
          if (memoryOpt)
            tid = Id(ts1, ts2);
          else
            tid = IdNaive(ts1, ts2, N);
        }
        free(levels);
        free(distance);
        free(que);
#ifdef DEBUGSYNC
        cout << "Automata is synchronizing." << endl;
#endif
        return false;
      }

      else {
        levels[distance[id]]++;
      }
    }
  }
  free(levels);
  free(distance);
  free(que);
#ifdef DEBUGSYNC
  cout << "Automata is not synchronizing." << endl;
#endif
  return true;
}

//This function is converting a base-decimal list to decimal number.
uint64_t getBaseID(unsigned short * list, int size, unsigned short base)
{
  int result = 0;
  for(int i = 0; i < size; i++)
  {
    result += list[i]*pow(base,i);
  }
  return result;
}

//This function is converting a decimal number to base-decimal list.
unsigned short* fromBaseID(int64_t num, int size, int base)
{
  unsigned short* path = new unsigned short[size];
  for(int i = 0; i < size; i++)
  {
    path[size-i-1] = num % base;
    num /= base;
  }

  return path;
}

//Printing automata accoring to letters with nextstates and outputs
void printAutomata(unsigned short ** nextStates, unsigned short ** outputs, int N, int p) {
  cout << "Automata ----------------------" << endl;
  for (int i = 0; i < p; ++i) {
    cout << "letter " << (char)(i + 97) << ":\t";
    for (int j = 0; j < N; ++j) {
      cout << nextStates[i][j] << "," << outputs[i][j] << "\t";
    }
    cout << endl;
  }
}

//Printing the homing sequence
void printPath(unsigned short* path, unsigned short size)
{
  cout << "Path is: ";
  for(int i = 0; i < size; i++)
  {
     cout << (char)(path[i] + 97) << " ";
  }
  cout << endl;
}

//Checking if the sequence is homing
bool isHomingSequence(unsigned short ** nextStates, unsigned short ** outputs, unsigned short N, unsigned short O, unsigned short * path, unsigned short length)
{
  //tempNextStates will store last states at the end
  //outputResults will store output sequences from each state according to input sequence
  unsigned short ** outputResults = new unsigned short*[N];
  unsigned short * tempNextStates = new unsigned short[N];

  //tempNextStates should start with all states as they are all active  
  for (int i = 0; i < N; i++)
  {
    outputResults[i] = new unsigned short[length];
    tempNextStates[i] = i;
  }

  //Filling the created elemenst above according to automata and input sequence
  for (int j = 0; j < length; j++)
  {
    for (int i = 0; i < N; i++)
    {
      outputResults[i][j] = outputs[path[j]][tempNextStates[i]];
      tempNextStates[i] = nextStates[path[j]][tempNextStates[i]];       
    }
  }

  //Checking the result if it is a homing sequence
  for(int i = 0; i < N; i++)
  {
    for(int j = i + 1; j < N; j++)
    {
      //To compare outputs, first we are acting like it is a length-decimal number and comparing it to decimal
    int a = getBaseID(outputResults[i],length,O);
    int b = getBaseID(outputResults[j],length,O);

    //cout << a << " " << b << endl;
      if(a == b)
      {
        //if we can get same output from different final states, then gg.
        if(tempNextStates[i] != tempNextStates[j])
        {
          for(int k = 0; k < N; k++)
          {
            free(outputResults[k]);
          }
          free(outputResults);
          free(tempNextStates);
          return false;
        }
      }
    }
  }
  for(int k = 0; k < N; k++)
  {
    free(outputResults[k]);
  }
  free(outputResults);
  free(tempNextStates);
  return true;
}

//Finding the shortest homing sequence with trying each sequence from shortest to longest till it finds one
unsigned short shortestHomingSequence(unsigned short ** nextStates, unsigned short ** outputs, unsigned short N, unsigned short P, bool debug, unsigned short O)
{
  int end = P;
  unsigned short size = 1;
  int limit = N*(N-1)/2 + 1;
  while(true)
  {

    for(int j = 0; j < end; j++)
    {
      //Creating path with converting j to size-decimal number.
      unsigned short * path = new unsigned short[size];
      path = fromBaseID(j, size, P);

      //if we found a result, this function can return
      if(isHomingSequence(nextStates, outputs, N, O, path, size))
      {
        if(debug)
        {
          printPath(path,size);
        }
        free(path);
        return size;
      }
    }

    end *= P;
    size++;
    //This part is unnecessary but we know that length of a hs cant be greater than n*(n-1)/2
    if(size == limit)
    {
      return size;
    }
  }
}

int shortestPath(int* a, int N, int P) {
  unsigned long long int noOfNodes = pow (2.0, double(N));
  unsigned long long int* distance = new unsigned long long int[noOfNodes];
  unsigned long long int* prev = new unsigned long long int[noOfNodes];
  unsigned long long int* letter = new unsigned long long int[noOfNodes];
  priority_queue<DistID<unsigned long long int> > que;

  for (unsigned long long int i = 0; i < noOfNodes; i++) {
    distance[i] =  ULLONG_MAX;
  }

  //dijkstra queue for the pairs
  distance[noOfNodes-1] = 0;
  prev[noOfNodes - 1] = noOfNodes - 1;
  que.push(DistID<unsigned long long int>(noOfNodes - 1, 0, 0));

  unsigned long long int * q_sN = new unsigned long long int[N];
  unsigned long long int * nextState = new unsigned long long int[N];
  //there are more nodes in the queue
  unsigned long long int counter = 1;
  while (!que.empty()) {
    unsigned long long int q_id = (que.top()).id; //ege
    que.pop();
    unsigned long long int q_dist = distance[q_id];
    unsigned long long int temp_q_id = q_id;
    //int bin_id = 0;
    
    //will process the pair with id q_id now
    for(unsigned long long int i = 0; i < N ; i++)
    {
      q_sN[i] = temp_q_id % 2;
      temp_q_id = temp_q_id >> 1;
    }

    for (unsigned long long int p = 0; p < P; p++) {
      memset(nextState, 0, sizeof(unsigned long long int) * N);

      for(unsigned long long int i = 0; i < N; i++)
      {
        if(q_sN[i] != 0)
        {
          nextState[a[i*P+p]] = 1; 
        }
        
      }
      unsigned long long int id = 0;
      for(unsigned long long int i = 0; i < N; i++)
      {
        if(nextState[i] == 1)
        {
          id += (1 << i);
        }
      }
      
      if (distance[id] > q_dist + 1)
      { 
        distance[id] = q_dist + 1; 
        prev[id] = q_id;
        letter[id] = p;
        que.push(DistID<unsigned long long int>(id, distance[id], counter++));
      }
    }
  }

  free(q_sN);
  free(nextState);

  unsigned long long int mindist = ULLONG_MAX;
  unsigned long long int minid;
  for (unsigned long long int i = 0; i < N; i++) {
    unsigned long long int pw = pow(2, i);
    if (distance[pw] < mindist) {
      mindist = distance[pw];
      minid = pw;
    }
  }

  unsigned long long int length = 0;
  unsigned long long int s = minid;
  unsigned long long int limit = pow(2, N) - 1;
#ifdef PRINTPATH
  cout << "Shortest Path: ";
#endif
  while (s < limit) {
    unsigned long long int p = letter[s];
#ifdef PRINTPATH
   cout << char(p + 97) << " ";
#endif   
    s = prev[s];
    length++;
  }
#ifdef PRINTPATH
   cout << endl;
#endif  

  free(distance);
  free(prev);
  free(letter);

  return length;
}

bool minimal(int N, int O, int P, unsigned short ** outputs, unsigned short ** nextStates, bool debug)
{
  //Initialization
  int divided = 0;
  unsigned short ** pi0 = new unsigned short * [N];
  unsigned short * baseIDs = new unsigned short[N];
  unsigned short ** IDlist = new unsigned short*[N];
  for(int i = 0; i < N; i++)
  {
    pi0[i] = new unsigned short[N+1];
    pi0[i][0] = 0;
    baseIDs[i] = pow(O,P);
    IDlist[i] = new unsigned short[P]; 
  }

  //giving ids to outputs to separate
  unsigned short * baseIDlist = new unsigned short[P];
  for(int j = 0; j < N; j++)
  {
    for(int i = 0; i < P; i++)
    {
      baseIDlist[i] = outputs[i][j];
    }
    long long int baseID = getBaseID(baseIDlist, P, O);
    bool put = false;
    //if they give the same output, they are in the same partition
    //else we gave new partition to them in put == false
    for(int i = 0; i < N; i++)
    {
      if(baseID == baseIDs[i])
      {
        put = true;
        pi0[i][0]++;
        pi0[i][pi0[i][0]] = j;
        break;
      }
    }

    if(put == false)
    {
      pi0[divided][1] = j;
      pi0[divided][0]++;
      baseIDs[divided] = baseID;
      divided++;
    }
  }

  //We are doing the same thing for next states in next iterations(If their next states are same they are in same partition.)

  for(int i = 0; i < N; i++)
  {
    baseIDs[i] = pow(N,P);
  }
  if(debug)
  {
    cout << "--------------pi0" << endl;
    for(int i = 0; i < divided; i++)
    {
      for(int j = 1; j <= pi0[i][0]; j++)
      {
        cout << pi0[i][j] << " ";
      }
      cout << endl;
    }
  }

  //It should be done till number of partitions == N or cant separate any partitions
  while(divided != N)
  {
    int prevDivided = divided;
    for(int i = 0; i < divided; i++)
    {
      if(pi0[i][0] != 1)
      { 
        for(int j = 1; j <= pi0[i][0]; j++)
        {       
          int current = pi0[i][j];
          for(int k = 0; k < P; k++)
          {
            int next = nextStates[k][current];
            for(int m = 0; m < divided; m++)
            {
              for(int n = 1; n <= pi0[m][0]; n++)
              {
                if(pi0[m][n] == next)
                {
                  IDlist[current][k] = m;
                }
              }
            } 
          }
        }
        uint64_t mainBaseID = getBaseID(IDlist[pi0[i][1]], P, N);
        for(int j = 2; j <= pi0[i][0]; j++)
        {
          uint64_t current = getBaseID(IDlist[pi0[i][j]], P, N);
          if(current == mainBaseID)
          {
            continue;
          }
          else
          {
            bool before = false;
            for(int k = prevDivided; k < divided; k++)
            {
              if(baseIDs[k] == current)
              {
                before = true;
                pi0[k][0]++;
                pi0[k][pi0[k][0]] = pi0[i][j];
                for(int m = j; m < pi0[i][0]; m++)
                {
                  pi0[i][m] = pi0[i][m+1];
                }
                pi0[i][0]--;
                j--;
                break;
              }
            }
            if(!before)
            {
              pi0[divided][1] = pi0[i][j];
              pi0[divided][0]++;
              baseIDs[divided] = current;
              divided++;
              for(int m = j; m < pi0[i][0]; m++)
              {
                pi0[i][m] = pi0[i][m+1];
              }
              j--;
              pi0[i][0]--;
            }
          }
        }
      }
      if(divided != prevDivided)
      {
        break;
      }
    }
    if(debug)
    {
      cout << "--------------piX" << endl;
      for(int i = 0; i < divided; i++)
      {
        for(int j = 1; j <= pi0[i][0]; j++)
        {
          cout << pi0[i][j] << " ";
        }
        cout << endl;
      }
    }

    if(divided == prevDivided)
    {
      free(baseIDs);
      free(baseIDlist);
      for(int j = 0; j < N; j++)
      {
        free(IDlist[j]);
        free(pi0[j]);
      }
      free(pi0);
      free(IDlist);
      return false;
    }
  }
  free(baseIDs);
  free(baseIDlist);
  for(int j = 0; j < N; j++)
  {
    free(IDlist[j]);
    free(pi0[j]);
  }
  free(pi0);
  free(IDlist);
  return true;
}

int main(int argc, char**argv)
{
  //Needed arguments to run this code
  if(argc < 5)
  {
    cout << "Usage: " << argv[0] << " no_states input_size output_size NBaseID OBaseID\n" << endl;
    cout << "Usage: " << argv[0] << " no_states input_size output_size rand_seed\n" << endl;
    cout << "Usage: " << argv[0] << " no_states input_size output_size \"all\"\n" << endl;
    cout << "Usage: " << argv[0] << " no_states input_size output_size \"noniso\"\n" << endl;
    return 0;
  }

  //Setup for the values from arguments
  unsigned short N = atoi(argv[1]);
  unsigned short P = atoi(argv[2]);
  unsigned short O = atoi(argv[3]);
  int * automata = new int [N*P];
  if(argc == 6)
  {
    uint64_t NBaseID = strtoll(argv[4], NULL, 10);
    uint64_t OBaseID = strtoll(argv[5], NULL, 10);
    //Hibbard Bound
    int limit = N*(N-1)/2;
    int wish = limit;
    //Berk Bound
    if(N > P)
    {
      wish = (P*(P-1))/2 + (N-P) * P -1;
      //wish = N-2;
    }
    cout << "The limit is " << limit << endl;
    cout << "The bound is " << wish << endl; 

    uint seed = atoi(argv[4]);

    unsigned short ** nextStates = new unsigned short*[P];
    unsigned short * oneDnextStates = new unsigned short[N*P];
    oneDnextStates= fromBaseID(NBaseID,(N*P), N);

    for(int i = 0; i < P; i++)
      {
          nextStates[i] = new unsigned short[N];
          for(int j = 0; j< N; j++)
          {
            automata[j*P + i] = oneDnextStates[i*N+j];
            nextStates[i][j] = oneDnextStates[i*N+j];
          }
      }

    int* inv_automata_ptrs = new int[P * (N + 1)];
    int* inv_automata = new int[P * N];

    for (int i = 0; i < P; ++i) {

    int *a = &(automata[i]);
    int *ia = &(inv_automata[i * N]);
    int *iap = &(inv_automata_ptrs[i * (N + 1)]);

    memset(iap, 0, sizeof(int) * (N + 1));
    for (int j = 0; j < N; j++) { iap[a[j * P] + 1]++; }
    for (int j = 1; j <= N; j++) { iap[j] += iap[j - 1]; }
    for (int j = 0; j < N; j++) { ia[iap[a[j * P]]++] = j; }
    for (int j = N; j > 0; j--) { iap[j] = iap[j - 1]; } iap[0] = 0;
  }
    
    if(synchronizing_check<true>(automata, inv_automata_ptrs, inv_automata, N, P))
    {
      int sP = shortestPath(automata, N, P);
      if(sP <= wish)
      {
      unsigned short ** tempoutputs = new unsigned short*[P];
      unsigned short * temponeDoutputs = new unsigned short[N*P];
      temponeDoutputs = fromBaseID(0,(N*P), O);
      for(int i = 0; i < P; i++)
      {
          tempoutputs[i] = new unsigned short[N];
          for(int j = 0; j< N; j++)
          {
            tempoutputs[i][j] = temponeDoutputs[i*N+j];
          }
      }
#ifdef PRINTAUTOMATA
        printAutomata(nextStates, tempoutputs, N, P);
#endif 
        free(tempoutputs);
        free(temponeDoutputs);
#ifdef SIZEOFPATH
        cout << "Shortest path is already shorter than or equal to bound(" << wish << ")." << endl << endl;
        return 0;
#endif
      }
    }

    unsigned short ** outputs = new unsigned short*[P];

    //Creating and filling 2 lists for next states and outputs acording to l and k respectively
    //We are connverting these l and p into O-decimal and N-decimal numbers and creating list with its digits
    unsigned short* oneDoutputs = new unsigned short[N*P];
          
    oneDoutputs = fromBaseID(OBaseID,(N*P), O);

          //Filling our matrices from the lists created above.
    for(int i = 0; i < P; i++)
    {
        outputs[i] = new unsigned short[N];
        for(int j = 0; j< N; j++)
        {
            outputs[i][j] = oneDoutputs[i*N+j];
        }
    }
#ifdef PRINTAUTOMATA
      printAutomata(nextStates, outputs, N, P);
#endif

    if(!minimal(N, O, P, outputs, nextStates, false))
    {
#ifdef SIZEOFPATH
      cout << "No homing sequence--Minimality check is failed." << endl;
#endif
      return 0;
    }

      //Results for randomly generated automata
      unsigned short size = shortestHomingSequence(nextStates, outputs, N, P, false, O);
      cout << "Size of path is: " << size << endl;

      for(int i = 0; i < P; i++)
      {
        free(outputs[i]);
        free(nextStates[i]); 
      }

      free(outputs);
      free(nextStates);
  }
  else if(strncmp(argv[4],"noniso",6) == 0)
  {
      //Hibbard Bound
      int limit = N*(N-1)/2;
      int wish = limit;
      int max = 0;
      //Berk Bound/Sad Story
      if(N > P)
      {
        wish = (P*(P-1))/2 + (N-P) * P;
        //wish = N-2;
      }
      cout << "The limit is " << limit << endl;
      cout << "The bound is " << wish << endl; 
      
      //list for holding sizes of homing sequences for each automaton created.
      long long int * sizes = new long long int[2];
      for(int k = 0; k < 2; k++)
      {
        sizes[k] = 0;
      }

      ifstream in;
      char* filename = new char[256];
      snprintf(filename, 256, "automatas_%d.txt", N);
      in.open(filename);
      if(in.fail())
      	cout << "Automata file with " << N << " states doesn't exist." << endl;
      string line;

      //For every combination of Edges
      while(!in.eof())
      {
        unsigned short ** nextStates = new unsigned short*[P];
        
        for(int i = 0; i < P; i++)
          {
              nextStates[i] = new unsigned short[N];
              getline(in,line);
              line = line.substr(line.rfind(':')+1);
              for(int j = 0; j< N; j++)
              {
                automata[j*P+i] = line.at(2*j+1) - '0';
                nextStates[i][j] = line.at(2*j+1) - '0';
              }
          }
        
        //inverse automata for synchronization check
        int* inv_automata_ptrs = new int[P * (N + 1)];
        int* inv_automata = new int[P * N];

        for (int i = 0; i < P; ++i) 
        {
          int *a = &(automata[i]);
          int *ia = &(inv_automata[i * N]);
          int *iap = &(inv_automata_ptrs[i * (N + 1)]);

          memset(iap, 0, sizeof(int) * (N + 1));
          for (int j = 0; j < N; j++) { iap[a[j * P] + 1]++; }
          for (int j = 1; j <= N; j++) { iap[j] += iap[j - 1]; }
          for (int j = 0; j < N; j++) { ia[iap[a[j * P]]++] = j; }
          for (int j = N; j > 0; j--) { iap[j] = iap[j - 1]; } iap[0] = 0;
        }
 
        if(synchronizing_check<true>(automata, inv_automata_ptrs, inv_automata, N, P))
        {
          //Try to find a shortest synchronizing sequence as shs can't be greater than syn. seq.'s length
          int sP = shortestPath(automata, N, P);
          if(sP <= wish)
          {
            sizes[0] += pow(O,N*P);
            unsigned short ** tempoutputs = new unsigned short*[P];
            unsigned short * temponeDoutputs = new unsigned short[N*P];
            temponeDoutputs = fromBaseID(0,(N*P), O);
            for(int i = 0; i < P; i++)
            {
                tempoutputs[i] = new unsigned short[N];
                for(int j = 0; j< N; j++)
                {
                  tempoutputs[i][j] = temponeDoutputs[i*N+j];
                }
            }
            
#ifdef PRINTAUTOMATA
            printAutomata(nextStates, tempoutputs, N, P);
#endif 
            free(tempoutputs);
            free(temponeDoutputs);
#ifdef SIZEOFPATH
            cout << "Shortest path is already shorter than or equal to bound(" << wish << ")." << endl << endl;
#endif
            continue;
          }
        }
        //If we couldn't find a shorter syn. seq. than our bound, we are still searching.
        //For every combination of outputs
        for(int l = 0; l < pow(O,N*P); l++)
        {
          //Creating 2 matrices for next states and outputs acording to inputs
          unsigned short ** outputs = new unsigned short*[P];

          //Creating and filling 2 lists for next states and outputs acording to l and k respectively
          //We are connverting these l and p into O-decimal and N-decimal numbers and creating list with its digits
          unsigned short * oneDoutputs = new unsigned short[N*P];
          
          oneDoutputs = fromBaseID(l,(N*P), O);

          //Filling our matrices from the lists created above.
          for(int i = 0; i < P; i++)
          {
              outputs[i] = new unsigned short[N];
              for(int j = 0; j< N; j++)
              {
                outputs[i][j] = oneDoutputs[i*N+j];
              }
          }
          
#ifdef PRINTAUTOMATA
          printAutomata(nextStates, outputs, N, P);
#endif
          
          if(!minimal(N, O, P, outputs, nextStates, false))
          {
#ifdef SIZEOFPATH
            cout << "No homing sequence--Minimality check is failed." << endl << endl;
#endif
            sizes[0]++;
            continue;
          } 
          //Result for current automata
          unsigned short size = shortestHomingSequence(nextStates, outputs, N, P, false, O);

          if(size > 8)
          {
          	if(size > max)
          	{
          		max = size;
          	}
#ifdef JUSTBOUNDS
            printAutomata(nextStates, outputs, N, P);
            sizes[1]++;
            cout << "Size of path is: " << size << endl;
            unsigned short size = shortestHomingSequence(nextStates, outputs, N, P, true, O);
            cout << endl;
#endif      
          }
          else
          {
            sizes[0]++;
#ifdef SIZEOFPATH
            cout << "Size of path is: " << size << endl << endl;
#endif
          }
          
          for(int i = 0; i < P; i++)
          {
            free(outputs[i]);
          }

          free(outputs);
          free(oneDoutputs);
            
        }
        for(int i = 0; i < P; i++)
        {
          free(nextStates[i]); 
        }   
        free(nextStates);   
      }

      //At the end we display the number of shs's for each size
    
      for(int i = 0; i < 2; i++)
      {
        if(i == 0)
        {
          cout << "There are " << sizes[i] << " FSM which are not falsify the bound." << endl;
        }
        else
        {             
          cout << "There are " << sizes[i] << " FSM falsify the bound." << endl;
        }
      }
      if(max == 0)
      	max = wish;
      cout << "Max size of shs is " << max << endl;
    }
  else if(strncmp(argv[4],"all",3) == 0)
  {
      //Hibbard Bound
      int limit = N*(N-1)/2;
      int wish = limit;
      int max = 0;
      //Berk Bound/Sad Story
      if(N > P)
      {
        wish = (P*(P-1))/2 + (N-P) * P ;
        //wish = N-2;
      }
      cout << "The limit is " << limit << endl;
      cout << "The bound is " << wish << endl; 
      
      //list for holding sizes of homing sequences for each automaton created.
      long long int * sizes = new long long int[2];
      for(int k = 0; k < 2; k++)
      {
        sizes[k] = 0;
      }

      //For every combination of Edges
      for(int k = 0; k < pow(N,N*P); k++)
      {
        unsigned short ** nextStates = new unsigned short*[P];
        unsigned short * oneDnextStates = new unsigned short[N*P];
        oneDnextStates= fromBaseID(k,(N*P), N);

        for(int i = 0; i < P; i++)
          {
              nextStates[i] = new unsigned short[N];
              for(int j = 0; j< N; j++)
              {
                automata[j*P+i] = oneDnextStates[i*N+j];
                nextStates[i][j] = oneDnextStates[i*N+j];
              }
          }

        //inverse automata for synchronization check
        int* inv_automata_ptrs = new int[P * (N + 1)];
        int* inv_automata = new int[P * N];

        for (int i = 0; i < P; ++i) 
        {
          int *a = &(automata[i]);
          int *ia = &(inv_automata[i * N]);
          int *iap = &(inv_automata_ptrs[i * (N + 1)]);

          memset(iap, 0, sizeof(int) * (N + 1));
          for (int j = 0; j < N; j++) { iap[a[j * P] + 1]++; }
          for (int j = 1; j <= N; j++) { iap[j] += iap[j - 1]; }
          for (int j = 0; j < N; j++) { ia[iap[a[j * P]]++] = j; }
          for (int j = N; j > 0; j--) { iap[j] = iap[j - 1]; } iap[0] = 0;
        }
 
        if(synchronizing_check<true>(automata, inv_automata_ptrs, inv_automata, N, P))
        {
          //Try to find a shortest synchronizing sequence as shs can't be greater than syn. seq.'s length
          int sP = shortestPath(automata, N, P);
          if(sP <= wish)
          {
            sizes[0] += pow(O,N*P);
            unsigned short ** tempoutputs = new unsigned short*[P];
            unsigned short * temponeDoutputs = new unsigned short[N*P];
            temponeDoutputs = fromBaseID(0,(N*P), O);
            for(int i = 0; i < P; i++)
            {
                tempoutputs[i] = new unsigned short[N];
                for(int j = 0; j< N; j++)
                {
                  tempoutputs[i][j] = temponeDoutputs[i*N+j];
                }
            }
#ifdef PRINTAUTOMATA
            printAutomata(nextStates, tempoutputs, N, P);
#endif 
            free(tempoutputs);
            free(temponeDoutputs);
#ifdef SIZEOFPATH
            cout << "Shortest path is already shorter than or equal to bound(" << wish << ")." << endl << endl;
#endif
            continue;
          }
        }
        //If we couldn't find a shorter syn. seq. than our bound, we are still searching.
        //For every combination of outputs
        for(int l = 0; l < pow(O,N*P); l++)
        {
          //Creating 2 matrices for next states and outputs acording to inputs
          unsigned short ** outputs = new unsigned short*[P];

          //Creating and filling 2 lists for next states and outputs acording to l and k respectively
          //We are connverting these l and p into O-decimal and N-decimal numbers and creating list with its digits
          unsigned short * oneDoutputs = new unsigned short[N*P];
          
          oneDoutputs = fromBaseID(l,(N*P), O);

          //Filling our matrices from the lists created above.
          for(int i = 0; i < P; i++)
          {
              outputs[i] = new unsigned short[N];
              for(int j = 0; j< N; j++)
              {
                outputs[i][j] = oneDoutputs[i*N+j];
              }
          }
#ifdef PRINTAUTOMATA
          printAutomata(nextStates, outputs, N, P);
#endif
          if(!minimal(N, O, P, outputs, nextStates, false))
          {
#ifdef SIZEOFPATH
            cout << "No homing sequence--Minimality check is failed." << endl << endl;
#endif
            sizes[0]++;
            continue;
          } 
          //Result for current automata
          unsigned short size = shortestHomingSequence(nextStates, outputs, N, P, false, O);

          if(size > wish-1)
          {
            if(size > max)
            {
              max = size;
            }
#ifdef JUSTBOUNDS
            printAutomata(nextStates, outputs, N, P);
            sizes[1]++;
            cout << "Size of path is: " << size << endl;
            unsigned short size = shortestHomingSequence(nextStates, outputs, N, P, true, O);
            cout << endl;
#endif   
          }
          else
          {
            sizes[0]++;
#ifdef SIZEOFPATH
            cout << "Size of path is: " << size << endl << endl;
#endif
          }
          
          for(int i = 0; i < P; i++)
          {
            free(outputs[i]);
          }

          free(outputs);
          free(oneDoutputs);
            
        }
        for(int i = 0; i < P; i++)
        {
          free(nextStates[i]); 
        }   
        free(nextStates);
        free(oneDnextStates);    
      }

      //At the end we display the number of shs's for each size
    
      for(int i = 0; i < 2; i++)
      {
        if(i == 0)
        {
          cout << "There are " << sizes[i] << " FSM which are not falsify the bound." << endl;
        }
        else
        {             
          cout << "There are " << sizes[i] << " FSM falsify the bound." << endl;
 		}     
      }
      if(max == 0)
        max = wish;
      cout << "Max size of shs is " << max << endl;
    }
  else
  {
    uint seed = atoi(argv[4]);

    //Creating 2 matrices for next states and outputs acording to inputs
    unsigned short ** outputs = new unsigned short*[P];
    unsigned short ** nextStates = new unsigned short*[P];

    //Randomly filling these matrices created above
    for (int i = 0; i < P; i++)
    {
      nextStates[i] = new unsigned short[N];
      outputs[i] = new unsigned short[N];
      for(int j = 0; j < N; j++)
      {
        nextStates[i][j] = ((unsigned short)rand_r(&seed)) % N;
        outputs[i][j] = ((unsigned short)rand_r(&seed)) % O;
      }
    }

#ifdef PRINTAUTOMATA
    printAutomata(nextStates, outputs, N, P);
#endif

    //Results for randomly generated automata
    unsigned short size = shortestHomingSequence(nextStates, outputs, N, P, false, O);
    cout << "Size of path is: " << size << endl;

    for(int i = 0; i < P; i++)
    {
      free(outputs[i]);
      free(nextStates[i]); 
    }

    free(outputs);
    free(nextStates);
  }
  return 0;
}