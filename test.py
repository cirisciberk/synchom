#!/usr/bin/python
import os
import sys
import subprocess
import csv
import pandas as pd

def tester(stateSizes, alphabetSizes, outputSizes, experimentSize):
    res=[]
    for s in stateSizes:
        for p in alphabetSizes:
            for o in outputSizes:
                for expID in range(experimentSize):
                    try:
                        res.append(subprocess.check_output(['./syncHom', str(s), str(p),str(o),str(expID)]))
                    except:
                        continue
    N,P,O,s,s_length,s_time,length,pair_time,greedy_time = [list(d) for d in zip(*[[int(i) for i in c.split(',')] for c in res])]
    df=pd.DataFrame(data={"N":N,"P":P,"O":O,"seed":s,"shortest length":s_length,"shortest time":s_time,"greedy length": length,"pair time":pair_time,"greedy time": greedy_time})
    df.to_csv("./experiment.csv",sep=',',index=False,columns=['N','P','O','seed','shortest length','shortest time','greedy length','pair time','greedy time'])
    data = pd.read_csv('experiment.csv')

if __name__ == "__main__":
    open("isHS.txt",'w').close() # empty the HS checking file.
    os.system("make -B syncHom")
    stateSizes = [32,64,128]
    alphabetSizes = [2,4,8]
    outputSizes = [2,4,8]
    experimentSize = 100
    tester(stateSizes, alphabetSizes, outputSizes, experimentSize)
