#include <iostream>
#include <fstream>
#include <stdint.h>
#include <math.h>
#include <cstring>
#include <chrono>

#define Id(s1, s2) ((s1 > s2)?(((s1 * (s1 + 1))/2) + s2):(((s2 * (s2 + 1))/2) + s1)) //this is how we compute the ids
#define s1fromId(id) ((int)(sqrt((2.0 * id) +1.0) - 0.5));
#define s2fromId(id, s1) (id - ((s1 * (s1 + 1))/2));
//#define PRINTAUTOMATA
//#define PRINTPAIR
//#define DEBUGPAIR
//#define PRINTPATH
//#define CHECKHS

using std::cout;
using std::endl;

struct PNode {
  int letter;
  PNode* next;
  PNode(int _letter, PNode* _next) : letter(_letter), next(_next) {}
};

uint64_t getBaseID(unsigned short * list, int size, unsigned short base)
{
  int result = 0;
  for(int i = 0; i < size; i++)
  {
    result += list[i]*pow(base,i);
  }
  return result;
}

//This function is converting a decimal number to base-decimal list.
int* fromBaseID(int64_t num, int size, int base)
{
  int* path = new int[size];
  for(int i = 0; i < size; i++)
  {
    path[size-i-1] = num % base;
    num /= base;
  }

  return path;
}

unsigned short getUniquePairID(unsigned short s1, unsigned short s2, int N)
{
  unsigned short result = 0;
  for(int i = 0; i < s1; i++)
  {
    result += (N-i-1);
  }

  result += (s2 - s1 -1);
  return result;
}

unsigned short* fromUniquePairID(unsigned short num, int N)
{
  unsigned short* pair = new unsigned short[2];
  if(num == N*(N-1)/2)
  {
    pair[0] = N;
    pair[1] = N;
    return pair;
  } 

  unsigned short compare = 0;
  unsigned short j = -1;
  unsigned short k;
  while(compare <= num)
  {
    j++;
    compare += (N-j-1);
  }

  compare -= (N-j-1);
  k = num - compare + j + 1;
  pair[0] = j;
  pair[1] = k;
  return pair;
}

void printFSM(unsigned short ** nextStates, unsigned short ** outputs, int N, int p) {
  cout << "Automata ----------------------" << endl;
  for (int i = 0; i < p; ++i) {
    cout << "letter " << (char)(i + 97) << ":\t";
    for (int j = 0; j < N; ++j) {
      cout << nextStates[i][j] << "," << outputs[i][j] << "\t";
    }
    cout << endl;
  }
}

void printPairAutomata(unsigned short ** pairAutomata, int N, int p) {
  cout << "Pair Automata ----------------------" << endl;
  for (int i = 0; i < p; i++) {
    cout << "letter " << (char)(i + 97) << ":\t";
    for (int j = 0; j < N; j++) {
      for(int k = j + 1; k < N; k++)
      {
        unsigned short * pairNS = fromUniquePairID(pairAutomata[i][getUniquePairID(j,k,N)], N);
        cout << j << "," << k << "/" << pairNS[0] << "," << pairNS[1] << "\t\t";
      }
      cout << endl;
    }
  }
}

unsigned short ** convertToPairAutomata(unsigned short N, unsigned short P, int noOfPair, unsigned short ** nextStates, unsigned short ** outputs)
{
  unsigned short ** pairAutomata = new unsigned short*[P];

  for (int i = 0; i < P; i++)
  {
    pairAutomata[i] = new unsigned short[noOfPair + 1];
  }

  for(int i = 0; i < P; i++)
  {
    pairAutomata[i][noOfPair] = noOfPair;
    for(int j = 0; j < N; j++)
    {
      for(int k = j+1; k < N; k++)
      {
        unsigned short id = getUniquePairID(j,k, N);

        if(nextStates[i][j] == nextStates[i][k] || outputs[i][j] != outputs[i][k])
        {
          pairAutomata[i][id] = noOfPair; 
        }
        else
        {
          unsigned short nS1 = nextStates[i][j];
          unsigned short nS2 = nextStates[i][k];
          if(nS1 > nS2)
          {
            unsigned short temp = nS1;
            nS1 = nS2;
            nS2 = temp;
          }

          pairAutomata[i][id] = getUniquePairID(nS1,nS2, N);
        }

#ifdef DEBUGPAIR
          cout << j << "," << k << "'s id is " << id << " and results id is " << pairAutomata[i][id] << endl;
#endif
      }
    }
  }
  return pairAutomata;
}

void insertToPath(int letter, PNode* &head, PNode* &last) {
  PNode* temp = new PNode(letter, NULL);
  if (head == NULL) {
    head = last = temp;
  } else {
    last = last->next = temp;
  }
}

char isHomingSequence(unsigned short ** nextStates, unsigned short ** outputs, unsigned short N, unsigned short O, int * path, unsigned short length)
{
  //tempNextStates will store last states at the end
  //outputResults will store output sequences from each state according to input sequence
  unsigned short ** outputResults = new unsigned short*[N];
  unsigned short * tempNextStates = new unsigned short[N];

  //tempNextStates should start with all states as they are all active  
  for (int i = 0; i < N; i++)
  {
    outputResults[i] = new unsigned short[length];
    tempNextStates[i] = i;
  }

  //Filling the created elemenst above according to automata and input sequence
  for (int j = 0; j < length; j++)
  {
    for (int i = 0; i < N; i++)
    {
      outputResults[i][j] = outputs[path[j]][tempNextStates[i]];
      tempNextStates[i] = nextStates[path[j]][tempNextStates[i]];       
    }
  }

  //Checking the result if it is a homing sequence
  for(int i = 0; i < N; i++)
  {
    for(int j = i + 1; j < N; j++)
    {
      //To compare outputs, first we are acting like it is a length-decimal number and comparing it to decimal
    int a = getBaseID(outputResults[i],length,O);
    int b = getBaseID(outputResults[j],length,O);

    //cout << a << " " << b << endl;
      if(a == b)
      {
        //if we can get same output from different final states, then gg.
        if(tempNextStates[i] != tempNextStates[j])
        {
          for(int k = 0; k < N; k++)
          {
            delete outputResults[k];
          }
          delete outputResults;
          delete tempNextStates;
          return 'F';
        }
      }
    }
  }
  for(int k = 0; k < N; k++)
  {
    delete outputResults[k];
  }
  delete outputResults;
  delete tempNextStates;
  return 'T';
}

unsigned short shortestHomingSequence(unsigned short ** nextStates, unsigned short ** outputs, unsigned short N, unsigned short P, unsigned short O)
{
  int end = P;
  unsigned short size = 1;
  int limit = N*(N-1)/2 + 1;
  while(true)
  {

    for(int j = 0; j < end; j++)
    {
      //Creating path with converting j to size-decimal number.
      int * path = new int[size];
      path = fromBaseID(j, size, P);

      //if we found a result, this function can return
      if(isHomingSequence(nextStates, outputs, N, O, path, size)=='T')
      {
        delete path ;
        return size;
      }
    }

    end *= P;
    size++;
    //This part is unnecessary but we know that length of a hs cant be greater than n*(n-1)/2
    if(size == limit)
    {
      return size;
    }
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////

void greedyHeuristic_naive(int* a, int* iap, int* ia, int N, int P, PNode* &path) {
  int noOfPair = (N * (N + 1)) / 2;
  int* actives = new int[N];
  int* active_marker = new int[N];

  int* distance = new int[noOfPair];
  int* next = new int[noOfPair];
  int* letter = new int[noOfPair];
  int* que = new int[noOfPair];
  
#ifdef TIMER
  double t1 = omp_get_wtime();
  double total = 0;
#endif
#if LOG_LEVEL == DATA_ANALYSIS
  int max_of_min_distances = 0;
  int min_distance_counter = 0;
  int min_distance_sum = 0;
#endif

  for (int i = 0; i < noOfPair; i++) {
    distance[i] =  -1;
  }

  //BFS queue for the pairs
  int qs = 0;
  int qe = 0;

  for (int i = 0; i < N; ++i) {
    int id = Id(i, i);
    distance[id] = 0;
    que[qe++] = id;
  }

  //there are more nodes in the queue
  while (qs < qe && qe < noOfPair) {
    int q_id = que[qs++];
    int q_dist = distance[q_id];

    //will process the pair with id q_id now
    int q_s1 = s1fromId(q_id); //the first state in the pair
    int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

#ifdef DEBUGS=-O3 -std=c++11
FILES= syncHom.cpp

syncHom: $(FILES)
  $(CC) $(FILES) $(CFLAGS) -o syncHom;
  @echo "syncHom is compiled" 
clean:
  rm -rf syncHom
    cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << q_dist << endl;
#endif

    for (int p = 0; p < P; p++) {
      int* p_ia = &ia[p * N]; //this is the inverse automata for letter p
      int* p_iap = &iap[p * (N + 1)]; //and its state pointers
      int iap_s1_limit = p_iap[q_s1 + 1];
      int iap_s2_limit = p_iap[q_s2 + 1];

      if (p_iap[q_s2] == iap_s2_limit) continue;

      for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < iap_s1_limit; ++iap_s1_ptr) {
        int ia_s1 = p_ia[iap_s1_ptr];
        for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < iap_s2_limit; ++iap_s2_ptr) {
          int ia_s2 = p_ia[iap_s2_ptr];
          int ia_id = Id(ia_s1, ia_s2);

          if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
            distance[ia_id] = q_dist + 1;

            next[ia_id] = q_id;
            letter[ia_id] = p;
            que[qe++] = ia_id;
          }
        }
      }
    }
  }

#ifdef TIMER
  double time = omp_get_wtime() - t1;
  total += time;
  cout << "BFS tree generation takes " << time << " seconds\n";
#if LOG_LEVEL == TIME_ANALYSIS
  out << total << ",";
#endif
#endif

  int* levels = new int[200];
  memset(levels, 0, 200 * sizeof(int));

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j <= i; ++j) {
      int id = Id(i, j);

      if (distance[id] == -1) {
        for (int p = 0; p < P; p++) {
          int ts1 = a[p * N + i];
          int ts2 = a[p * N + j];

          int tid = Id(ts1, ts2);
          //cout << "tid " <<  tid << ": distance is " << distance[tid] << endl;
        }

        //cout << "automaton is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
        exit(1);
      } else {
        levels[distance[id]]++;
      }
    }
  }

#ifdef DEBUG
  int lvl = 0;
  while (levels[lvl] > 0) {
    cout << "lvl " << lvl++ << ": " << levels[lvl] << endl;
  }
  cout << endl;
#endif

#ifdef TIMER
  t1 = omp_get_wtime();
#endif

  PNode* last = NULL;
  memset(active_marker, 0, sizeof(int) * N);

  int no_actives = N;
  for (int i = 0; i < N; ++i) {
    actives[i] = i;
  }

  int step = 1;
  while (no_actives > 1) {
    //cout << "no active states is " << no_actives << endl;
    //find the pair id with minimum distance
    int min_distance = N * N;
    int min_id;
    for (int i = 0; i < no_actives; i++) {
      for (int j = 0; j < i; j++) {
        int s1 = actives[i];
        int s2 = actives[j];

        int id = Id(s1, s2);

        if (min_distance > distance[id]) {
          min_distance = distance[id];
          min_id = id;
          if (min_distance == 1) break;
        }
      }
      if (min_distance == 1) break;
    }

#if LOG_LEVEL == DATA_ANALYSIS
    if (max_of_min_distances < distance[min_id])
      max_of_min_distances = distance[min_id];
    min_distance_counter++;
    min_distance_sum += distance[min_id];
#endif
    // cout << "merging pair from level " << min_distance << endl;

    //apply the path and store it
    int pid = min_id;
    while (distance[pid] > 0) {
      int let = letter[pid];
      insertToPath(let, path, last);

      for (int i = 0; i < no_actives; i++) {
        actives[i] = a[let * N + actives[i]];
      }
      pid = next[pid];
    }

    //reduce the number of active states
    int active_count = 0;
    for (int i = 0; i < no_actives; i++) {
      int act = actives[i];
      if (active_marker[act] != step) {
        actives[active_count++] = act;
        active_marker[act] = step;
      }
    }
    no_actives = active_count;
    step++;
  }

#ifdef TIMER
  time = omp_get_wtime() - t1;
//cout << "Path generation takes " << time << " seconds\n";
  total += time;
  cout << "The heuristic takes " << total << " seconds\n";
#if LOG_LEVEL == TIME_ANALYSIS
  out << total << ",";
#endif
#endif

  delete distance;
  delete next;
  delete letter;
  delete que;
  delete actives;
  delete active_marker;
}

///////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char**argv)
{

  if(argc < 5)
  {
    cout << "Usage: " << argv[0] << " no_states input_size output_size rand_seed\n" << endl;
    return 0;
  }

  typedef std::chrono::steady_clock::time_point Time; 

  //Setup for the values from arguments
  unsigned short N = atoi(argv[1]);
  unsigned short P = atoi(argv[2]);
  unsigned short O = atoi(argv[3]);
  uint seed = atoi(argv[4]);
  uint sd=seed;

  //Creating 2 matrices for next states and outputs acording to inputs
  unsigned short ** outputs = new unsigned short*[P];
  unsigned short ** nextStates = new unsigned short*[P];

  //Randomly filling these matrices created above
  for (int i = 0; i < P; i++)
  {
    nextStates[i] = new unsigned short[N];
    outputs[i] = new unsigned short[N];
    for(int j = 0; j < N; j++)
    {
      nextStates[i][j] = ((unsigned short)rand_r(&seed)) % N;
      outputs[i][j] = ((unsigned short)rand_r(&seed)) % O;
    }
  }


  /* 







        YOUR CODE GOES HERE



  */


  int noOfPair = N*(N-1)/2;
  Time pair_start = std::chrono::steady_clock::now();
  unsigned short ** pairAutomata = convertToPairAutomata(N, P, noOfPair, nextStates, outputs);
  Time pair_end = std::chrono::steady_clock::now();

#ifdef PRINTAUTOMATA
  printFSM(nextStates, outputs, N, P);
  cout << endl;
#endif

#ifdef PRINTPAIR
  printPairAutomata(pairAutomata, N, P);
  cout << endl;
#endif

  int* automata= new int[(noOfPair+1)*P];

  int i=0;
  for(int j=0;j<P;++j){
    for(int k=0;k<noOfPair+1;++k){
      automata[i++]=pairAutomata[j][k];
    }
  }
  
/////////////////////////////////// GREEDY /////////////////////////////////////////

  PNode* path = NULL;
  unsigned short N_=noOfPair+1;
  int* inv_automata_ptrs = new int[P * (N_+1)];
  int* inv_automata = new int[P * N_];

  for (int i = 0; i < P; ++i) {
		int *a = &(automata[i * N_]);
		int* ia = &(inv_automata[i * N_]);
		int* iap = &(inv_automata_ptrs[i * (N_ + 1)]);

		memset(iap, 0, sizeof(int) * (N_ + 1));
		for (int j = 0; j < N_; j++) {iap[a[j] + 1]++;}
		for (int j = 1; j <= N_; j++) {iap[j] += iap[j - 1];}
		for (int j = 0; j < N_; j++) {ia[iap[a[j]]++] = j;}
		for (int j = N_; j > 0; j--) {iap[j] = iap[j - 1];} iap[0] = 0;
	}

  Time greedy_start=std::chrono::steady_clock::now();
  greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N_, P, path);
  Time greedy_end=std::chrono::steady_clock::now();

	PNode* pnode = path;
	int plength = 0;
	while (pnode) {
    #ifdef PRINTPATH
		  cout << (char)(pnode->letter+97) << " ";
    #endif
		pnode = pnode->next;
		plength++;
	}
  
  #ifdef CHECKHS
    std::ofstream checkhs;
    checkhs.open("isHS.txt",std::ios_base::out | std::ios_base::app);
    int* sh_path= new int [plength];
    for(int j=0;j<plength;++j){
      sh_path[j]=path->letter;
      path=path->next;
    }
    if(isHomingSequence(nextStates,outputs,N,O,sh_path,plength)=='F')
      checkhs << isHomingSequence(nextStates,outputs,N,O,sh_path,plength);
    checkhs.close();
  #endif

  Time shortest_start=std::chrono::steady_clock::now();
  int shortest_length=shortestHomingSequence(nextStates,outputs,N,P,O);
  Time shortest_end=std::chrono::steady_clock::now();
  int shortestHS_time=std::chrono::duration_cast<std::chrono::microseconds>(shortest_end - shortest_start).count();

  for(int i = 0; i < P; i++)
  {
    delete outputs[i];
    delete nextStates[i]; 
  }

  delete outputs;
  delete nextStates;
  delete automata;

  int greedy_time=std::chrono::duration_cast<std::chrono::microseconds>(greedy_end - greedy_start).count();
  int pair_time=std::chrono::duration_cast<std::chrono::microseconds>(pair_end - pair_start).count();

  cout<<N<<","<<P<<","<<O<<","<<sd<<","<<shortest_length<<","<<shortestHS_time<<","<<plength<<","<<pair_time<<","<<greedy_time;

  return 0;
}